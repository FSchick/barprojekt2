import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { TabsPage } from '../pages/tabs/tabs';
import { AddTobaccoPageModule } from '../pages/add-tobacco/add-tobacco.module';
import { AddSpeisePageModule } from '../pages/add-speise/add-speise.module';
import { AddGetraenkPageModule } from '../pages/add-getraenk/add-getraenk.module';
import { TabaccoDetailsPageModule } from '../pages/tabacco-details/tabacco-details.module';
import { BestellungsDetailsPageModule } from '../pages/bestellungs-details/bestellungs-details.module';
import { BestellungsAufnahmePageModule } from '../pages/bestellungs-aufnahme/bestellungs-aufnahme.module';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DataService } from '../providers/data-service/data-service';
import {LoginPageModule} from "../pages/login/login.module";

import {AdminPageModule} from "../pages/admin/admin.module";
import {NutzeranlegenPageModule} from "../pages/nutzeranlegen/nutzeranlegen.module";
import {BenutzerVerwaltungPageModule} from "../pages/benutzer-verwaltung/benutzer-verwaltung.module";
import {TischeVerwaltenPageModule} from "../pages/tische-verwalten/tische-verwalten.module";
import {BestellungenPageModule} from "../pages/bestellungen/bestellungen.module";
import {ProduktePageModule} from "../pages/produkte/produkte.module";
import {KuecheBarPageModule} from "../pages/kueche-bar/kueche-bar.module";
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import {StornoPageModule} from "../pages/storno/storno.module";
import {SchichtenAnlegenPageModule} from "../pages/schichten-anlegen/schichten-anlegen.module";
import {SchichtenVerwaltenPageModule} from "../pages/schichten-verwalten/schichten-verwalten.module";
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { NativeStorage } from '@ionic-native/native-storage';
import {TagesUmsatzPageModule} from "../pages/tages-umsatz/tages-umsatz.module";
import {KundenAnsichtPageModule} from "../pages/kunden-ansicht/kunden-ansicht.module";
import { Vibration } from '@ionic-native/vibration';
import { NativeAudio } from '@ionic-native/native-audio';
import { Geolocation } from '@ionic-native/geolocation';
import {KellnerAbrechnungPageModule} from "../pages/kellner-abrechnung/kellner-abrechnung.module";
import { NativeRingtones } from '@ionic-native/native-ringtones';
import { Deeplinks } from '@ionic-native/deeplinks';
import {Nav} from 'ionic-angular';
import {UpdateGetraenkPageModule} from "../pages/update-getraenk/update-getraenk.module";
import {UpdateSpeisePageModule} from "../pages/update-speise/update-speise.module";
import {UpdateTobaccoPageModule} from "../pages/update-tobacco/update-tobacco.module";
import { Geofence } from '@ionic-native/geofence';



@NgModule({
  declarations: [
    MyApp,
    TabsPage,
  ],
  imports: [
    BrowserModule,
    AddGetraenkPageModule,
    AddSpeisePageModule,
    AddTobaccoPageModule,
    AdminPageModule,
    BenutzerVerwaltungPageModule,
    BestellungenPageModule,
    BestellungsAufnahmePageModule,
    BestellungsDetailsPageModule,
    KuecheBarPageModule,
    LoginPageModule,
    NutzeranlegenPageModule,
    ProduktePageModule,
    TabaccoDetailsPageModule,
    TischeVerwaltenPageModule,
    StornoPageModule,
    SchichtenAnlegenPageModule,
    SchichtenVerwaltenPageModule,
    TagesUmsatzPageModule,
    KundenAnsichtPageModule,
    KellnerAbrechnungPageModule,
    UpdateGetraenkPageModule,
    UpdateSpeisePageModule,
    UpdateTobaccoPageModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataService,
    AuthServiceProvider,
    BarcodeScanner,
    NativeStorage,
    Vibration,
    NativeAudio,
    Geolocation,
    NativeRingtones,
    Deeplinks,
    Geofence,
    Nav
  ]
})
export class AppModule {}
