import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {LoginPage} from "../pages/login/login";
import { Deeplinks } from '@ionic-native/deeplinks';
import {AdminPage} from "../pages/admin/admin";
import {BestellungenPage} from "../pages/bestellungen/bestellungen";
import {ProduktePage} from "../pages/produkte/produkte";
import {KuecheBarPage} from "../pages/kueche-bar/kueche-bar";
import {TabsPage} from "../pages/tabs/tabs";
import {BenutzerVerwaltungPage} from "../pages/benutzer-verwaltung/benutzer-verwaltung";
import {KellnerAbrechnungPage} from "../pages/kellner-abrechnung/kellner-abrechnung";
import {NutzeranlegenPage} from "../pages/nutzeranlegen/nutzeranlegen";
import {SchichtenAnlegenPage} from "../pages/schichten-anlegen/schichten-anlegen";
import {SchichtenVerwaltenPage} from "../pages/schichten-verwalten/schichten-verwalten";
import {StornoPage} from "../pages/storno/storno";
import {TagesUmsatzPage} from "../pages/tages-umsatz/tages-umsatz";
import {TischeVerwaltenPage} from "../pages/tische-verwalten/tische-verwalten";
import {BestellungsDetailsPage} from "../pages/bestellungs-details/bestellungs-details";
import {AddTobaccoPage} from "../pages/add-tobacco/add-tobacco";
import {AddGetraenkPage} from "../pages/add-getraenk/add-getraenk";
import {AddSpeisePage} from "../pages/add-speise/add-speise";
import {UpdateTobaccoPage} from "../pages/update-tobacco/update-tobacco";
import {UpdateSpeisePage} from "../pages/update-speise/update-speise";
import {UpdateGetraenkPage} from "../pages/update-getraenk/update-getraenk";
import {TabaccoDetailsPage} from "../pages/tabacco-details/tabacco-details";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav:Nav;
  rootPage:any = LoginPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private deeplinks: Deeplinks) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      this.deeplinks.routeWithNavController(this.nav, {
        '/tabs': TabsPage,
        '/tabs/admin': AdminPage,
        '/tabs/admin/benutzer-verwaltung': BenutzerVerwaltungPage,
        '/tabs/admin/kellner-abrechnung': KellnerAbrechnungPage,
        '/tabs/admin/nutzer-anlegen': NutzeranlegenPage,
        '/tabs/admin/schichten-verwalten': SchichtenVerwaltenPage,
        '/tabs/admin/schichten-verwalten/schichten-anlegen': SchichtenAnlegenPage,
        '/tabs/admin/storno': StornoPage,
        '/tabs/admin/tages-umsatz': TagesUmsatzPage,
        '/tabs/admin/tische-verwalten': TischeVerwaltenPage,
        '/tabs/bestellungen': BestellungenPage,
        '/tabs/bestellungen/bestellungs-details/:keyOfItem': BestellungsDetailsPage,
        '/tabs/produkte': ProduktePage,
        '/tabs/produkte/add-tobacco': AddTobaccoPage,
        '/tabs/produkte/add-getaenk': AddGetraenkPage,
        '/tabs/produkte/add-speise': AddSpeisePage,
        '/tabs/produkte/update-tobacco/:tobaccoId': UpdateTobaccoPage,
        '/tabs/produkte/update-getaenk/:getraenkeId': UpdateGetraenkPage,
        '/tabs/produkte/update-speise/:speiseId': UpdateSpeisePage,
        '/tabs/produkte/tobacco-details/:tobaccoId': TabaccoDetailsPage,
        '/tabs/kueche-bar': KuecheBarPage
      }).subscribe((match) => {
        // match.$route - the route we matched, which is the matched entry from the arguments to route()
        // match.$args - the args passed in the link
        // match.$link - the full link data
        console.log('Successfully matched route', match);
      }, (nomatch) => {
        // nomatch.$link - the full link data
        console.error('Got a deeplink that didn\'t match', nomatch);
      });
    });
  }
}
