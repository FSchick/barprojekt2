import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DataService} from "../../providers/data-service/data-service";

/**
 * Generated class for the UpdateTobaccoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage({
  segment: '/tabs/produkte/update-tobacco/:tobaccoId'
})
@Component({
  selector: 'page-update-tobacco',
  templateUrl: 'update-tobacco.html',
})
export class UpdateTobaccoPage {
  tobaccoId;
  tobaccoName;
  tobaccoDecription;
  tobaccoHashtag;
  tabaccoPreis;
  tobaccoAvailable = true;
  tobaccoObject;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataService) {
      this.tobaccoId = this.navParams.get("tobaccoId")
    this.tobaccoObject = this.getTobaccoObject(this.tobaccoId)
      this.tobaccoName = this.tobaccoObject.name;
      this.tabaccoPreis = this.tobaccoObject.price;
      this.tobaccoHashtag = this.generateString(this.tobaccoObject.hashtags);
      this.tobaccoDecription = this.tobaccoObject.description;
      this.tobaccoAvailable = this.tobaccoObject.visible;
  }

  getTobaccoObject(tobaccoID){
    for(var i = 0; i<this.data.tobaccoList.length; i++){
      if(this.data.tobaccoList[i].id == tobaccoID){
        return this.data.tobaccoList[i];
      }
    }
  }

  writeNewTobacco() {
    if(this.tobaccoName!= null && this.tobaccoName != '' && this.tobaccoName != ' '){
      this.data.writeOrUpdateTobacco(this.tobaccoId, this.tobaccoName, this.tobaccoDecription, this.generateArray(this.tobaccoHashtag), this.tabaccoPreis.replace(',','.'), this.tobaccoAvailable);
      this.navCtrl.pop();
    } else{
      alert('Bitte Namen angeben!')
    }
  }

  /**
   * Erzeugt aus einem String einen Array mit Werten
   *
   * @param string
   * @returns {string[]|any}
   */
  generateArray(string){
    var stringWithoutSpace = string.replace(/\s/g,'');
    return stringWithoutSpace.split(",");
  }

  /**
   * Erstellt aus dem Array mit Hashtags einen zusammenhängenden String.
   *
   * @param hashtags
   * @returns {string}
   */
  generateString(hashtags){
    var str = '';
    var length = hashtags.length;

    for(var k = 0; k<(hashtags.length-1); k++){
      str += hashtags[k] + ', '
    }
    str+=hashtags[length-1];
    return str;
  }
}
