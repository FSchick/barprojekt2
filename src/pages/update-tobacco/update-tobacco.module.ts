import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateTobaccoPage } from './update-tobacco';

@NgModule({
  declarations: [
    UpdateTobaccoPage,
  ],
  imports: [
    IonicPageModule.forChild(UpdateTobaccoPage),
  ],
  exports: [
    UpdateTobaccoPage
  ]
})
export class UpdateTobaccoPageModule {}
