import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabaccoDetailsPage } from './tabacco-details';

@NgModule({
  declarations: [
    TabaccoDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(TabaccoDetailsPage),
  ],
  exports: [
    TabaccoDetailsPage
  ]
})
export class TabaccoDetailsPageModule {}
