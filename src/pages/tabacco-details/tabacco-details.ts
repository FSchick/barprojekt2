import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {DataService} from "../../providers/data-service/data-service";

/**
 * Zeigt Eigenschaften eines Tabaks an.
 */
@IonicPage({
  segment: '/tabs/produkte/tobacco-details/:tobaccoId'
})
@Component({
  selector: 'page-tabacco-details',
  templateUrl: 'tabacco-details.html',
})
export class TabaccoDetailsPage {
  tobacco;
  tobaccoId;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataService) {
    this.tobaccoId = navParams.get('tobaccoId');
    this.tobacco = this.getTobaccoObject(this.tobaccoId)
  }

  getTobaccoObject(tobaccoID){
    for(var i = 0; i<this.data.tobaccoList.length; i++){
      if(this.data.tobaccoList[i].id == tobaccoID){
        return this.data.tobaccoList[i];
      }
    }
  }

}
