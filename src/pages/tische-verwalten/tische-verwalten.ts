import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {DataService} from "../../providers/data-service/data-service";

/**
 * Erlaubt es einem Administrator die Tische zu verwalten. Es können Tische manuell auf bestezt gesetzt werden und wieder freigegeben werden. Weiterhin kann ein neuer Tisch hinzugefügt werden.
 */
@IonicPage({
  segment: '/tabs/admin/tische-verwalten'
})
@Component({
  selector: 'page-tische-verwalten',
  templateUrl: 'tische-verwalten.html',
})
export class TischeVerwaltenPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataService) {
  }

  addTisch() {
    this.data.writeNewTisch(this.data.tischList.length)
  }

  aendereTischstatus(item) {
    item.verwendbar = !item.verwendbar;
    this.data.aendereTischstatus(item);
  }


}
