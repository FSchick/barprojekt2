import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TischeVerwaltenPage } from './tische-verwalten';

@NgModule({
  declarations: [
    TischeVerwaltenPage,
  ],
  imports: [
    IonicPageModule.forChild(TischeVerwaltenPage),
  ],
  exports: [
    TischeVerwaltenPage
  ]
})
export class TischeVerwaltenPageModule {}
