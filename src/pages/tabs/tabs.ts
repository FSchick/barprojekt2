import { Component } from '@angular/core';

import {AdminPage} from "../admin/admin";
import {DataService} from "../../providers/data-service/data-service";
import {BestellungenPage} from "../bestellungen/bestellungen";
import {ProduktePage} from "../produkte/produkte";
import {KuecheBarPage} from "../kueche-bar/kueche-bar";
import {NavController} from "ionic-angular";
import {KundenAnsichtPage} from "../kunden-ansicht/kunden-ansicht";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = ProduktePage;
  tab2Root = BestellungenPage;
  tab3Root = KuecheBarPage;
  tab4Root = AdminPage;
  tab5Root = KundenAnsichtPage;

  constructor(private data: DataService,  public navCtrl: NavController) {

  }

  checkUserPermission(seite){
    var rolle = this.data.currentUser.rolle;
    if(rolle == 'kunde' && seite == 'kunde'){
      return true;
    }
    if(seite == "produkte" && rolle == "admin"){
      return true;
    }
    else if(seite == "bestellungen" && (rolle == "admin" || rolle == "kassierer")){
      return true;
    }
    else if(seite == "küchebar" && rolle != 'kunde'){
      return true;
    }
    else if(seite == "admin" && rolle == "admin"){
      return true;
    } else {
      return false;
    }
  }

  clearPage(){
    this.navCtrl.getActiveChildNav().popAll();
  }
}
