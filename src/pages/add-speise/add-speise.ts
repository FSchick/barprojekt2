import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {DataService} from "../../providers/data-service/data-service";

/**
 *
 * Der Code behandelt das Hinzufügen einer Speise zur Datenbank.
 */
@IonicPage({
  segment: '/tabs/produkte/add-speise'
})
@Component({
  selector: 'page-add-speise',
  templateUrl: 'add-speise.html',
})
export class AddSpeisePage {
  speiseName;
  speisePreis;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataService) {

  }

  writeNewSpeise() {
    if (this.speiseName != null && this.speiseName != '' && this.speiseName != ' ') {
      this.data.writeOrUpdateSpeise(null, this.speiseName, this.speisePreis.replace(',', '.'));
      this.navCtrl.pop();
    } else {
      alert('Bitte Namen angeben!')
    }
  }

}
