import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddSpeisePage } from './add-speise';

@NgModule({
  declarations: [
    AddSpeisePage,
  ],
  imports: [
    IonicPageModule.forChild(AddSpeisePage),
  ],
  exports: [
    AddSpeisePage
  ]
})
export class AddSpeisePageModule {}
