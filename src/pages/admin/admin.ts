import {Component} from '@angular/core';
import {App, IonicPage, NavController, NavParams} from 'ionic-angular';
import {NutzeranlegenPage} from "../nutzeranlegen/nutzeranlegen";
import {DataService} from "../../providers/data-service/data-service";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {TischeVerwaltenPage} from "../tische-verwalten/tische-verwalten";
import {BenutzerVerwaltungPage} from "../benutzer-verwaltung/benutzer-verwaltung";
import {StornoPage} from "../storno/storno";
import {SchichtenVerwaltenPage} from "../schichten-verwalten/schichten-verwalten";
import {TagesUmsatzPage} from "../tages-umsatz/tages-umsatz";
import {KellnerAbrechnungPage} from "../kellner-abrechnung/kellner-abrechnung";

/**
 *
 * Weiterleitungen auf die verschiedenen Seiten des Administrator Bereiches.
 *
 */


@IonicPage({
  segment: '/tabs/admin'
})
@Component({
  selector: 'page-admin',
  templateUrl: 'admin.html',
})


export class AdminPage {

  constructor(private _app: App, public navCtrl: NavController, public navParams: NavParams, private data: DataService,
              private authService: AuthServiceProvider) {

  }

  addUser() {
    this.navCtrl.push(NutzeranlegenPage)
  }

  editAccess() {
    this.navCtrl.push(BenutzerVerwaltungPage)
  }

  addTable() {
    this.navCtrl.push(TischeVerwaltenPage)
  }

  showStornos() {
    this.navCtrl.push(StornoPage)
  }

  showSchichten() {
    this.navCtrl.push(SchichtenVerwaltenPage);
  }

  logout() {
    this.authService.logout().then(() => {
      const root = this._app.getRootNav();
      root.popToRoot();
    });
  }

  showTagesUmsatz() {
    this.navCtrl.push(TagesUmsatzPage)
  }

  showKellnerAbrechnungsPage() {
    this.navCtrl.push(KellnerAbrechnungPage);
  }
}
