import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KellnerAbrechnungPage } from './kellner-abrechnung';

@NgModule({
  declarations: [
    KellnerAbrechnungPage,
  ],
  imports: [
    IonicPageModule.forChild(KellnerAbrechnungPage),
  ],
  exports: [
    KellnerAbrechnungPage
  ]
})
export class KellnerAbrechnungPageModule {}
