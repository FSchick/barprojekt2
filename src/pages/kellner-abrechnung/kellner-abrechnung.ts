import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {DataService} from "../../providers/data-service/data-service";

/**
 *
 * Behandelt das Abrechnen der Kasse eines Kellners.
 *
 */
@IonicPage({
  segment: '/tabs/admin/kellner-abrechnung'
})
@Component({
  selector: 'page-kellner-abrechnung',
  templateUrl: 'kellner-abrechnung.html',
})
export class KellnerAbrechnungPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private data: DataService, private alertCtrl: AlertController) {
  }

  abrechnenKellner(kellner) {
    var that = this;
    var array = [];
    var einnahmen = 0;
    this.data.getUmsatzKellner(kellner).then((res) => {
      array = JSON.parse(JSON.stringify(res));
      for (var i = 0; i < array.length; i++) {
        for (var k = 0; k < array[i].einzelposten.length; k++) {
          if (array[i].einzelposten[k].abrechnungKellner != null && typeof array[i].einzelposten[k].abrechnungKellner != 'undefined') {
            for (var l = 0; l < array[i].einzelposten[k].abrechnungKellner.length; l++) {
              if (array[i].einzelposten[k].abrechnungKellner[l].name == kellner && !array[i].einzelposten[k].abrechnungKellner[l].abgerechnet) {
                einnahmen += array[i].einzelposten[k].abrechnungKellner[l].summe
              }
            }
          }
        }
      }
      let alert = this.alertCtrl.create({
        title: 'Abrechnung',
        message: kellner + '<br/>' + einnahmen.toFixed(2),
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Abrechnen',
            handler: () => {
              console.log(JSON.stringify(array));
              for (var i = 0; i < array.length; i++) {
                for (var k = 0; k < array[i].einzelposten.length; k++) {
                  if (array[i].einzelposten[k].abrechnungKellner != null && typeof array[i].einzelposten[k].abrechnungKellner != 'undefined') {
                    for (var l = 0; l < array[i].einzelposten[k].abrechnungKellner.length; l++) {
                      if (array[i].einzelposten[k].abrechnungKellner[l].name == kellner) {
                        array[i].einzelposten[k].abrechnungKellner[l].abgerechnet = true;
                        console.log('auf abgerechnet gesetzt')
                      }
                    }
                  }
                }
              }

              for (var i = 0; i < array.length; i++) {
                var gesamtAbgerechnet = true;
                for (var k = 0; k < array[i].einzelposten.length; k++) {
                  if (array[i].einzelposten[k].abrechnungKellner != null && typeof array[i].einzelposten[k].abrechnungKellner != 'undefined') {
                    for (var l = 0; l < array[i].einzelposten[k].abrechnungKellner.length; l++) {
                      if (!array[i].einzelposten[k].abrechnungKellner[l].abgerechnet) {
                        console.log(false);
                        gesamtAbgerechnet = false;
                      }
                    }
                  } else {
                    gesamtAbgerechnet = false;
                  }
                }
                array[i].alleKellnerAbgerechnet = gesamtAbgerechnet;
              }

              for (var z = 0; z < array.length; z++) {
                that.data.updateBestellung(array[z]);
              }
              this.navCtrl.pop();
            }
          }
        ]
      });
      alert.present();
    });
  }
}
