import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KuecheBarPage } from './kueche-bar';

@NgModule({
  declarations: [
    KuecheBarPage,
  ],
  imports: [
    IonicPageModule.forChild(KuecheBarPage),
  ],
  exports: [
    KuecheBarPage
  ]
})
export class KuecheBarPageModule {}
