import { Component } from '@angular/core';
import {App, IonicPage, NavController, AlertController} from 'ionic-angular';
import {DataService} from "../../providers/data-service/data-service";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";

/**
 *
 * Diese Seite ist für die Anzeige der zu bearbeitenden Einzelposten in der Küche/Bar da.
 *
 */
@IonicPage({
  segment: '/tabs/kueche-bar'
})
@Component({
  selector: 'page-kueche-bar',
  templateUrl: 'kueche-bar.html',
})
export class KuecheBarPage {

  constructor(private _app: App, public navCtrl: NavController, private data: DataService,
              private authService: AuthServiceProvider, public alertCtrl: AlertController) {

  }

  setBestellungPrepared(item, bestellung){
    bestellung.anzahlBearbeitet = bestellung.anzahl;
    bestellung.bearbeitet = true;
    //wird an dieser Stelle auf true gesetzt. Falls nicht alle Einzelposten bearbeitet sind wird es zurückgesetzt
    item.bearbeitet = true;
    for(var i = 0; i<item.einzelposten.length; i++){
      if(item.einzelposten[i].anzahlBearbeitet != item.einzelposten[i].anzahl){
        item.bearbeitet = false;
      }
    }
    this.data.updateBestellung(item);
  }

  setBestellungMinusOne(item, bestellung){
    bestellung.anzahlBearbeitet += 1;
    if(bestellung.anzahlBearbeitet == bestellung.anzahl){
      bestellung.bearbeitet = true;
    }
    //wird an dieser stelle auf true gesetzt. falls nicht alle einzelposten bearbeitet sind wird es zurückgesetzt
    item.bearbeitet = true;
    for(var i = 0; i<item.einzelposten.length; i++){
      if(item.einzelposten[i].anzahlBearbeitet != item.einzelposten[i].anzahl){
        item.bearbeitet = false;
      }
    }
    this.data.updateBestellung(item);
  }

  isRelevantForUser(bestellung){
    if(this.data.currentUser.rolle=='admin'){
      return true;
    } else if((this.data.currentUser.rolle == 'bar' || this.data.currentUser.rolle == 'kassierer') && (bestellung.typ =='speise' || bestellung.typ=='getraenk')){
      return true;
    } else if(this.data.currentUser.rolle == 'küche' && bestellung.typ =='shisha'){
      return true;
    } else{
      return false;
    }
  }

  logout(){
    this.authService.logout().then(()=>{
      const root = this._app.getRootNav();
      root.popToRoot();
    });
  }

  setTabakEmpty(bestellung){
    var that = this;
    let confirm = this.alertCtrl.create({
      title: 'Tabak auf "Leer" setzen',
      message: 'Wollen Sie diesen Tabak wirklich auf "Leer" setzen?',
      buttons: [
        {
          text: 'Nein',
          handler: () => {

          }
        },
        {
          text: 'Ja',
          handler: () => {
            that.data.getTobaccoByName(bestellung.name).then((tobacco)=>{
              var tabak: any;
              tabak = tobacco;
              if(tobacco != null){
                tabak.visible = false;
                that.data.updateTobaccoEigenschaften(tabak);
              }
            });
          }
        }
      ]
    });
    confirm.present();
  }

  editAll(item){
    for(var i = 0; i < item.einzelposten.length; i++){
      if(this.data.currentUser.rolle == 'admin'){
        item.einzelposten[i].bearbeitet = true;
        item.einzelposten[i].anzahlBearbeitet = item.einzelposten[i].anzahl;
      } else if((this.data.currentUser.rolle == 'kassierer' || this.data.currentUser.rolle == 'bar') && (item.einzelposten[i].typ == 'getraenk' || item.einzelposten[i].typ == 'speise')){
        item.einzelposten[i].bearbeitet = true;
        item.einzelposten[i].anzahlBearbeitet = item.einzelposten[i].anzahl;
      } else if(this.data.currentUser.rolle == 'küche' && item.einzelposten[i].typ == 'shisha'){
        item.einzelposten[i].bearbeitet = true;
        item.einzelposten[i].anzahlBearbeitet = item.einzelposten[i].anzahl;
      }
    }
    console.log(JSON.stringify(item));
    item.bearbeitet = true;
    for(var i = 0; i<item.einzelposten.length; i++){
      if(item.einzelposten[i].anzahlBearbeitet != item.einzelposten[i].anzahl){
        item.bearbeitet = false;
      }
    }
    this.data.updateBestellung(item);
  }
}
