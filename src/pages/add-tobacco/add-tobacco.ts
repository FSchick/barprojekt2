import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DataService} from "../../providers/data-service/data-service";

/**
 *
 * Diese Seite behandelt das Hinzufügen eines Tabaks zur Datenbank.
 *
 */
@IonicPage({
  segment: '/tabs/produkte/add-tobacco'
})
@Component({
  selector: 'page-add-tobacco',
  templateUrl: 'add-tobacco.html',
})
export class AddTobaccoPage {
  tobaccoName;
  tobaccoDecription;
  tobaccoHashtag;
  tabaccoPreis;
  tobaccoAvailable = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataService) {

  }

  writeNewTobacco() {
    if(this.tobaccoName!= null && this.tobaccoName != '' && this.tobaccoName != ' '){
      this.data.writeOrUpdateTobacco(null, this.tobaccoName, this.tobaccoDecription, this.generateArray(this.tobaccoHashtag), this.tabaccoPreis.replace(',','.'), this.tobaccoAvailable);
      this.navCtrl.pop();
    } else{
      alert('Bitte Namen angeben!')
    }
  }

  /**
   * Erzeugt aus einem String einen Array mit Werten
   *
   * @param string
   * @returns {string[]|any}
   */
  generateArray(string){
    var stringWithoutSpace = string.replace(/\s/g,'');
    return stringWithoutSpace.split(",");
  }

  /**
   * Erstellt aus dem Array mit Hashtags einen zusammenhängenden String.
   *
   * @param hashtags
   * @returns {string}
   */
  generateString(hashtags){
    var str = '';
    var length = hashtags.length;

    for(var k = 0; k<(hashtags.length-1); k++){
      str += hashtags[k] + ', '
    }
    str+=hashtags[length-1];
    return str;
  }


}
