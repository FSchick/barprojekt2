import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddTobaccoPage } from './add-tobacco';

@NgModule({
  declarations: [
    AddTobaccoPage,
  ],
  imports: [
    IonicPageModule.forChild(AddTobaccoPage),
  ],
  exports: [
    AddTobaccoPage
  ]
})
export class AddTobaccoPageModule {}
