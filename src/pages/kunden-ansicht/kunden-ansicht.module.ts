import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KundenAnsichtPage } from './kunden-ansicht';

@NgModule({
  declarations: [
    KundenAnsichtPage,
  ],
  imports: [
    IonicPageModule.forChild(KundenAnsichtPage),
  ],
  exports: [
    KundenAnsichtPage
  ]
})
export class KundenAnsichtPageModule {}
