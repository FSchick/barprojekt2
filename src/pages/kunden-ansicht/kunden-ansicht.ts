import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import {DataService} from "../../providers/data-service/data-service";
import { Geofence } from '@ionic-native/geofence';


import { Geolocation } from '@ionic-native/geolocation';

/**
 * Auf dieser Seite kann ein Kunde alle Bars sehen, die "Partner-Bars" dieser App sind.
 */
const geofenceString = 'barApp_Geofence_'
@IonicPage()
@Component({
  selector: 'page-kunden-ansicht',
  templateUrl: 'kunden-ansicht.html',
})


export class KundenAnsichtPage {

  private position = {
    lat: null,
    lng: null
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, private barcodeScanner: BarcodeScanner,
              private data: DataService, private geolocation: Geolocation, private geofence: Geofence) {
    geofence.initialize().then(
      // resolved promise does not return a value
      () => console.log('Geofence Plugin Ready'),
      (err) => console.log(err)
    )
    this.loadData();
    console.log(geofenceString)
  }

  loadData(){
      this.data.getBars().then((bool)=>{
        console.log(this.data.barList.length);
        for(var i = 0; i<this.data.barList.length; i++){
          this.berechneEntfernungBar(this.data.barList[i], i);
          console.log(this.data.barList[i])
        }
      })
  }

  makeCode () {
    this.barcodeScanner.encode('TEXT_TYPE', this.data.currentUser.id).then(() => {
    }, (err) => {
      alert(err);
    });
  }




  returnToFixed(str){
    return ' ' + parseFloat(str).toFixed(2) + ' ';
  }


  private berechneEntfernungBar(bar, cnt){
    var that = this;
    if(this.position.lat == null || this.position.lng == null){
      this.geolocation.getCurrentPosition().then((resp) => {
        that.position.lat = resp.coords.latitude;
        that.position.lng = resp.coords.longitude;
        var lat1 = resp.coords.latitude;
        var lat2 = bar.adresse.lat;
        var lng1 = resp.coords.longitude;
        var lng2 = bar.adresse.lng;
        var lat = (lat1 + lat2) / 2 * 0.01745;
        var  dx = 111.3 * Math.cos(lat) * (lng1 - lng2);
        var dy = 111.3 * (lat1 - lat2);
        that.data.barList[cnt].entfernung = (Math.sqrt(dx * dx + dy * dy));
        that.addGeofence(that.data.barList[cnt].adresse.lat, that.data.barList[cnt].adresse.lng, that.data.barList[cnt].name)
      }).catch((error) => {
        console.log(error);
        return ('?')
      });
    } else {
      var lat1 = that.position.lat;
      var lat2 = bar.adresse.lat;
      var lng1 = that.position.lng;
      var lng2 = bar.adresse.lng;
      var lat = (lat1 + lat2) / 2 * 0.01745;
      var dx = 111.3 * Math.cos(lat) * (lng1 - lng2);
      var dy = 111.3 * (lat1 - lat2);
      that.data.barList[cnt].entfernung = (Math.sqrt(dx * dx + dy * dy));
      that.addGeofence(that.data.barList[cnt].adresse.lat, that.data.barList[cnt].adresse.lng, that.data.barList[cnt].name)
    }

  }

  private addGeofence(lat, lng, barname) {
    //options describing geofence
    let fence = {
      id: geofenceString + barname, //any unique ID
      latitude:       lat, //center of geofence radius
      longitude:      lng,
      radius:         3000, //radius to edge of geofence in meters
      transitionType: 3, //see 'Transition Types' below
      notification: { //notification settings
        id:             1, //any unique ID
        title:          'You crossed a fence' + barname, //notification title
        text:           'You just arrived to Gliwice city center.', //notification body
        openAppOnClick: true //open app when notification is tapped
      }
    }

    this.geofence.addOrUpdate(fence).then(
      () => console.log('Geofence added'),
      (err) => console.log('Geofence failed to add')
    );
  }
}
