import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProduktePage } from './produkte';

@NgModule({
  declarations: [
    ProduktePage,
  ],
  imports: [
    IonicPageModule.forChild(ProduktePage),
  ],
  exports: [
    ProduktePage
  ]
})
export class ProduktePageModule {}
