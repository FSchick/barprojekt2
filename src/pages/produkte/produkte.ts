import { Component } from '@angular/core';
import {App, IonicPage, NavController, Platform} from 'ionic-angular';
import { DataService } from '../../providers/data-service/data-service';
import { AddTobaccoPage } from '../add-tobacco/add-tobacco';
import {TabaccoDetailsPage} from "../tabacco-details/tabacco-details";
import {AddGetraenkPage} from "../add-getraenk/add-getraenk";
import {AddSpeisePage} from "../add-speise/add-speise";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {UpdateTobaccoPage} from "../update-tobacco/update-tobacco";
import {UpdateSpeisePage} from "../update-speise/update-speise";
import {UpdateGetraenkPage} from "../update-getraenk/update-getraenk";

/**
 * Zeigt dem Admisitrator alle Verfügbaren Produkte an. Sie können bearbeitet und gelöscht werden.
 * Weiterhin gibt es die Möglichkeit neue Produkte anzulegen.
 */
@IonicPage({
  segment: '/tabs/produkte'
})
@Component({
  selector: 'page-produkte',
  templateUrl: 'produkte.html',
})
export class ProduktePage {

  constructor(private _app: App, public navCtrl: NavController, private data:DataService,
              private authService: AuthServiceProvider) {
  }

  addTobacco(){
    this.navCtrl.push(AddTobaccoPage);
  }

  addGetraenk(){
    this.navCtrl.push(AddGetraenkPage);
  }

  addSpeise(){this.navCtrl.push(AddSpeisePage);
  }

  showTobaccoDetails(item){
    this.navCtrl.push(TabaccoDetailsPage, {
      tobaccoId: item.id
    });
  }

  deleteTobacco(id){
    this.data.deleteTobacco(id);
  }

  changeSpeise(item){
    this.navCtrl.push(UpdateSpeisePage, {
      speiseId: item.id
    });
  }

  changeTobacco(item){
    this.navCtrl.push(UpdateTobaccoPage, {
      tobaccoId: item.id
    });
  }

  changeGetraenk(item){
    this.navCtrl.push(UpdateGetraenkPage, {
      getraenkeId: item.id
    });
  }

  logout(){
    this.authService.logout().then(()=>{
      const root = this._app.getRootNav();
      root.popToRoot();
    });
  }
}
