import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SchichtenVerwaltenPage } from './schichten-verwalten';

@NgModule({
  declarations: [
    SchichtenVerwaltenPage,
  ],
  imports: [
    IonicPageModule.forChild(SchichtenVerwaltenPage),
  ],
  exports: [
    SchichtenVerwaltenPage
  ]
})
export class SchichtenVerwaltenPageModule {}
