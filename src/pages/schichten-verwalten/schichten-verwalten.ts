import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {SchichtenAnlegenPage} from "../schichten-anlegen/schichten-anlegen";
import {DataService} from "../../providers/data-service/data-service";

/**
 * Zeigt dem Administrator alle Schichten an, die Eingetragen wurden. Weiterhin kann eine neue Schicht angelegt werden.
 * Dafür wird allerdings auf die Seiten schichten-anlegen gepusht.
 */
@IonicPage({
  segment: '/tabs/admin/schichten-verwalten'
})
@Component({
  selector: 'page-schichten-verwalten',
  templateUrl: 'schichten-verwalten.html',
})
export class SchichtenVerwaltenPage {
  nextHeader;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataService) {
  }

  addSchicht() {
    this.navCtrl.push(SchichtenAnlegenPage), {
      schicht: null
    };
  }

  getNextHEader(schicht) {
    var schichtStringArray = schicht.startDate.split("-");
    var strSchichtDateFormatted = schichtStringArray[1] + "-" + schichtStringArray[2] + "-" + schichtStringArray[0];
    var index = this.data.schichtenList.indexOf(schicht);

    if (index == 0) {
      var header = schichtStringArray[2] + "-" + schichtStringArray[1] + "-" + schichtStringArray[0];
      this.nextHeader = header;
      return true;
    } else {
      var strArrayBefore = this.data.schichtenList[index - 1].startDate.split("-");
      var strBefore = strArrayBefore[1] + "-" + strArrayBefore[2] + "-" + strArrayBefore[0];

      if (new Date(strSchichtDateFormatted) > new Date(strBefore)) {
        var header = schichtStringArray[2] + "-" + schichtStringArray[1] + "-" + schichtStringArray[0];
        this.nextHeader = header;
        return true;
      } else {
        return false;
      }
    }
  }

  changeSchicht(schichtToEdit) {
    this.navCtrl.push(SchichtenAnlegenPage, {
      schichtToEdit: schichtToEdit
    });
  }

}
