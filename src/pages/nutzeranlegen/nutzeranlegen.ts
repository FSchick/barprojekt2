import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {DataService} from "../../providers/data-service/data-service";

/**
 * Auf dieser Seite können Neue Nutzer angelegt werden. Dabei können auch Berechtigungen vergeben werden.
 */
@IonicPage({
  segment: '/tabs/admin/nutzer-anlegen'
})
@Component({
  selector: 'page-nutzeranlegen',
  templateUrl: 'nutzeranlegen.html',
})
export class NutzeranlegenPage {

  private username;
  private password;
  private rolle;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataService) {
  }

  addUser() {
    this.data.addUser(this.username.toLowerCase(), this.password, this.rolle, 0).then((bool) => {
      if (bool) {
        this.navCtrl.pop();
      } else {
        alert("Anlegen des Nutzers fehlgeschlagen");
      }
    })
  }

}
