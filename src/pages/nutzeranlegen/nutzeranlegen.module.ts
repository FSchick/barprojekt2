import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NutzeranlegenPage } from './nutzeranlegen';

@NgModule({
  declarations: [
    NutzeranlegenPage,
  ],
  imports: [
    IonicPageModule.forChild(NutzeranlegenPage),
  ],
  exports: [
    NutzeranlegenPage
  ]
})
export class NutzeranlegenPageModule {}
