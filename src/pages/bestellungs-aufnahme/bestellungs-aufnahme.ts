import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {DataService} from "../../providers/data-service/data-service";

/**
 *
 * Behandelt die genaue Logik, wie Posten zu der Bestellung hinzgefügt werden.
 * Auch wird hier die Suchlogik abgebildet um Gentränke etc. zu finden.
 *
 */
@IonicPage()
@Component({
  selector: 'page-bestellungs-aufnahme',
  templateUrl: 'bestellungs-aufnahme.html',
})
export class BestellungsAufnahmePage {
  item;
  bestellungsListe = [];
  tisch;
  filteredTobaccoList;
  filteredDrinkList;
  filteredSpeiseList;
  searchStringTobacco = '';
  searchStringDrink = '';
  searchStringSpeise = '';
  bestellungExistiert;
  advanedTobaccoSearchActivated = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataService, public alertCtrl: AlertController) {
    this.tisch = navParams.get('tisch');
    this.bestellungExistiert = navParams.get('zuBestellunghHizufuegen');
    this.item = this.navParams.get('bestellung')
  }


  /**
   * Fügt einen Posten zu der Bestellung hinzu.
   *
   * @param item
   * @param typ
   * @param kommentar
   */
  addBestellung(item, typ, kommentar) {
    //umweg, um mit Kopie des Objektes zu arbeiten
    var bestellung = JSON.parse(JSON.stringify(item));
    if (!bestellung.visible) {
      alert("Der Tabak ist nicht verfügbar")
    } else {
      bestellung.typ = typ;
      bestellung.bearbeitet = false;
      bestellung.anzahlBearbeitet = 0;
      //diese Parameter werden auf null gesetzt, damit sie nicht mit in die Datenbank aufgenommen werden
      bestellung.abgerechnet = false;
      bestellung.id = null;
      bestellung.hashtags = null;
      bestellung.description = null;
      bestellung.visible = null;
      bestellung.anzahlBezahlt = 0;
      bestellung.anzahlInRechnung = 0;
      if (this.item != null && typeof this.item != 'undefined') {
        bestellung.id = this.item.einzelposten.length + this.bestellungsListe.length;
      } else {
        bestellung.id = this.bestellungsListe.length;
      }

      bestellung.kommentar = kommentar;


      if (this.bestellungsListe.length == 0) {
        bestellung.anzahl = 1;
        this.bestellungsListe.push(bestellung);
      } else {
        for (var i = 0; i < this.bestellungsListe.length; i++) {
          if (this.bestellungsListe[i].name == bestellung.name && bestellung.kommentar == null && this.bestellungsListe[i].kommentar == null) {
            this.bestellungsListe[i].anzahl += 1;
            break;
          } else if (i == (this.bestellungsListe.length - 1) && this.bestellungsListe[i].name != bestellung.name) {
            bestellung.anzahl = 1;
            this.bestellungsListe.push(bestellung);
            break;
          } else if (bestellung.kommentar != null) {
            bestellung.anzahl = 1;
            this.bestellungsListe.push(bestellung);
            break;
          }
        }
      }
    }
  }

  /**
   * Ein Kommentar für einen extra Wunsch zu einem Posten hinzzufügen.
   *
   * @param item
   * @param type
   */
  addComment(item, type) {
    let alerter = this.alertCtrl.create({
      title: 'Kommentar hinzufügen',
      inputs: [
        {
          name: 'kommentar',
          placeholder: 'Kommentar'
        },
        {
          name: 'preis',
          value: '0',
          type: 'number'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
          }
        },
        {
          text: 'Bestätigen',
          handler: data => {
            var itemK = JSON.parse(JSON.stringify(item));
            if ((parseFloat(itemK.price) + parseFloat(data.preis)) < 0) {
              alert('Es sind keine negativen Gesamtpreise möglich!');
            } else {
              itemK.price = parseFloat(itemK.price) + parseFloat(data.preis);
              itemK.name += '*';
              this.addBestellung(itemK, type, data.kommentar)
            }
          }
        }
      ]
    });
    alerter.present();
  }

  /**
   * Erstellt die Bestellung und sendet diese ab.
   *
   */
  createBestellung() {
    if (this.tisch.publicID == null) {
      alert("Tisch-Nr angeben!")
    } else {
      if (this.bestellungExistiert) {
        for (var i = 0; i < this.bestellungsListe.length; i++) {
          if (this.itemExistsInEinzelposten(this.bestellungsListe[i])) {
            this.item.einzelposten[this.getItemIndex(this.bestellungsListe[i])].anzahl += this.bestellungsListe[i].anzahl;
            this.item.einzelposten[this.getItemIndex(this.bestellungsListe[i])].bearbeitet = false;
            this.item.einzelposten[this.getItemIndex(this.bestellungsListe[i])].bezahlt = false;
          } else {
            this.item.einzelposten.push(this.bestellungsListe[i])
          }
        }
        for (var i = 0; i < this.bestellungsListe.length; i++) {
          if (this.bestellungsListe[i].anzahl > 0) {
            this.bestellungsListe[i].anzahlInRechnung = this.bestellungsListe[i].anzahl;
          }
        }
        this.item.bearbeitet = false;
        this.data.updateBestellung(this.item);
        this.navCtrl.pop();
      } else {
        for (var i = 0; i < this.bestellungsListe.length; i++) {
          this.bestellungsListe[i].anzahlInRechnung = this.bestellungsListe[i].anzahl;
        }
        this.data.createBestellungGlobal(this.tisch.publicID, this.bestellungsListe);
        this.tisch.besetzt = true;
        this.data.aendereTischstatus(this.tisch);
        const index = this.navCtrl.getActive().index;
        this.navCtrl.remove(1, index);
        this.bestellungsListe = [];
      }
    }
  }

  getItemIndex(bestellung) {
    for (var j = 0; j < this.item.einzelposten.length; j++) {
      if (bestellung.name == this.item.einzelposten[j].name) {
        return j;
      }
    }
  }

  itemExistsInEinzelposten(bestellung) {
    for (var j = 0; j < this.item.einzelposten.length; j++) {
      if (bestellung.name == this.item.einzelposten[j].name) {
        return true;
      }
    }
    return false;
  }

  /**
   * Sucht in der Liste der Tabaksorten nach dem zur Suche passenden Sorten.
   */
  getFilteredTobaccos() {
    if (!this.advanedTobaccoSearchActivated) {
      this.searchStringTobacco += "";
      var list = [];
      for (var i = 0; i < this.data.tobaccoList.length; i++) {
        if (this.data.tobaccoList[i].name.toLowerCase().includes(this.searchStringTobacco.toLowerCase())) {
          list.push(JSON.parse(JSON.stringify(this.data.tobaccoList[i])));
        }
      }
      this.filteredTobaccoList = list;
    } else {
      this.searchStringTobacco += "";
      var list = [];
      var hashtagList = this.searchStringTobacco.replace(/ /g, "").split(",");
      if (hashtagList[hashtagList.length - 1] == '') {
        hashtagList.pop();
      }
      for (var i = 0; i < this.data.tobaccoList.length; i++) {
        if (this.checkIfMatchesHashtags(this.data.tobaccoList[i], hashtagList)) {
          list.push(this.data.tobaccoList[i])
        }
      }
      this.filteredTobaccoList = list;
    }
  }

  /**
   * Prüft ob die Suche zu den Hashtags eines Tabak-Eintrages passt.
   *
   * @param tobacco
   * @param hashtagList
   * @returns {boolean}
   */
  private checkIfMatchesHashtags(tobacco, hashtagList) {
    var resultArray = [];
    for (var i = 0; i < hashtagList.length; i++) {
      resultArray.push(false);
    }

    for (var j = 0; j < hashtagList.length; j++) {
      for (var k = 0; k < tobacco.hashtags.length; k++) {
        if (tobacco.hashtags[k].toLowerCase().includes(hashtagList[j].toLowerCase())) {
          resultArray[j] = true;
          continue;
        }
      }
    }

    for (var l = 0; l < resultArray.length; l++) {
      if (!resultArray[l]) {
        return false;
      }
    }

    return true;
  }

  /**
   * Suche für die Liste der Getränke.
   */
  getFilteredDrinks() {
    this.searchStringDrink += "";
    if (this.searchStringDrink == '') {
      this.filteredDrinkList = [];
    }
    var list = [];
    for (var i = 0; i < this.data.getraenkeList.length; i++) {
      if (this.data.getraenkeList[i].name.toLowerCase().includes(this.searchStringDrink.toLowerCase())) {
        list.push(JSON.parse(JSON.stringify(this.data.getraenkeList[i])));
      }
    }
    this.filteredDrinkList = list;
  }

  /**
   * Suche für die Liste der Speisen.
   */
  getFilteredSepisen() {
    this.searchStringSpeise += "";
    var list = [];
    for (var i = 0; i < this.data.speiseList.length; i++) {
      if (this.data.speiseList[i].name.toLowerCase().includes(this.searchStringSpeise.toLowerCase())) {
        list.push(JSON.parse(JSON.stringify(this.data.speiseList[i])));
      }
    }
    this.filteredSpeiseList = list;
  }

  changeAdvancedTobaccoSearchMode() {
    this.advanedTobaccoSearchActivated = !this.advanedTobaccoSearchActivated;
  }

  /**
   * Zeigt genaue Informationen über einen Tabak in einem PopUp an.
   * @param item
   */
  showInformation(item) {
    var str = "<h5>Beschreibung:</h5>" + "<br/>" + item.description + "<br/>" + "<br/>" + "<h5>Hashtags:</h5>";
    for (var i = 0; i < item.hashtags.length; i++) {
      str += item.hashtags[i] + "<br/>"
    }

    let alert = this.alertCtrl.create({
      title: item.name,
      subTitle: str,
      buttons: ['OK']
    });
    alert.present();
  }

  deletePosten(item) {
    console.log(item);
    var index = this.bestellungsListe.indexOf(item);
    item.anzahl -= 1;
    if (item.anzahl == 0) {
      this.bestellungsListe.splice(index, 1);
      console.log(this.bestellungsListe);
      for (var i = 0; i < this.bestellungsListe.length; i++) {
        this.bestellungsListe[i].id = i;
      }
    }
  }
}

