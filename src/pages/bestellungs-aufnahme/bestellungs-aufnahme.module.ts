import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BestellungsAufnahmePage } from './bestellungs-aufnahme';

@NgModule({
  declarations: [
    BestellungsAufnahmePage,
  ],
  imports: [
    IonicPageModule.forChild(BestellungsAufnahmePage),
  ],
  exports: [
    BestellungsAufnahmePage
  ]
})
export class BestellungsAufnahmePageModule {}
