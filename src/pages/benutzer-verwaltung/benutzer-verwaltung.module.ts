import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BenutzerVerwaltungPage } from './benutzer-verwaltung';

@NgModule({
  declarations: [
    BenutzerVerwaltungPage,
  ],
  imports: [
    IonicPageModule.forChild(BenutzerVerwaltungPage),
  ],
  exports: [
    BenutzerVerwaltungPage
  ]
})
export class BenutzerVerwaltungPageModule {}
