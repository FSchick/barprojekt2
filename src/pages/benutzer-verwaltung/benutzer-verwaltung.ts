import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {DataService} from "../../providers/data-service/data-service";

/**
 *
 * Verwaltung der bestehenden Nutzer.
 * Nutzer können gelöscht oder deren Berechtigungen angpeasst werden.
 *
 */
@IonicPage({
  segment: '/tabs/admin/benutzer-verwaltung'
})
@Component({
  selector: 'page-benutzer-verwaltung',
  templateUrl: 'benutzer-verwaltung.html',
})
export class BenutzerVerwaltungPage {


  constructor(public navCtrl: NavController, public navParams: NavParams, private data:DataService, public alertCtrl:AlertController) {
  }

  deleteUser(user){
    let confirm = this.alertCtrl.create({
      title: 'Benutzer Löschen',
      message: 'Wollen Sie diesen Benutzer wirklich löschen?',
      buttons: [
        {
          text: 'Nein',
        },
        {
          text: 'Ja',
          handler: () => {
            this.data.deleteBenutzer(user).then((deleted)=>{
              if(deleted){
                alert("Der Benutzer " + user.email +  " wurde gelöscht")
              } else {
                alert("Es ist ein Fehler aufgetreten!")
              }
            })
          }
        }
      ]
    });
    confirm.present();
  }

  changeRole(user){
    var that = this;
    let alertWin = this.alertCtrl.create();
    alertWin.setTitle('Rolle anpassen');

    alertWin.addInput({
      type: 'radio',
      label: 'Admin',
      value: 'admin',
      checked: that.userHasPermission('admin', user)
    });

    alertWin.addInput({
      type: 'radio',
      label: 'Bedienung',
      value: 'kassierer',
      checked: that.userHasPermission('kassierer', user)
    });

    alertWin.addInput({
      type: 'radio',
      label: 'Küche',
      value: 'küche',
      checked: that.userHasPermission('küche', user)
    });

    alertWin.addInput({
      type: 'radio',
      label: 'Bar',
      value: 'bar',
      checked: that.userHasPermission('bar', user)
    });

    alertWin.addButton('Cancel');
    alertWin.addButton({
      text: 'OK',
      handler: data => {
        user.rolle = data;
        that.data.aendereBenutzerRolle(user).then((roleChanged)=>{
          if(roleChanged){
            alert("Die Berechtigungen wurden angepasst");
          }
        })
      }
    });
    alertWin.present();
  }

  userHasPermission(permission, user){
    return (permission == user.rolle)
  }

}
