import {Component} from "@angular/core";
import {AlertController, IonicPage, NavController, NavParams} from "ionic-angular";
import {DataService} from "../../providers/data-service/data-service";
import {BestellungsAufnahmePage} from "../bestellungs-aufnahme/bestellungs-aufnahme";
import {BarcodeScanner} from "@ionic-native/barcode-scanner";

/**
 * Generated class for the BestellungsDetailsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage({
  segment: '/tabs/bestellungen/bestellungs-details/:keyOfItem'
})
@Component({
  selector: 'page-bestellungs-details',
  templateUrl: 'bestellungs-details.html',
})
export class BestellungsDetailsPage {
  keyOfItem;
  item;
  einzelpostenAbrechnungAktiv = false;
  einzelPostenListe = [];
  einzelPostenZuBezahlen = [];
  aktuellerTisch;
  dontDestroyPage = false;


  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, private data: DataService,
              private barcodeScanner: BarcodeScanner) {
    this.keyOfItem = this.navParams.get('keyOfItem');
    this.item = this.getItemFromDataService(this.keyOfItem)
    this.data.getTischByPublicID(this.item.tischNr).then((res) => {
      this.aktuellerTisch = res;
    });
  }

  getItemFromDataService(keyOfItem){
    for(var i = 0; i<this.data.bestellungsListeOffenChronologisch.length; i++){
      if(this.data.bestellungsListeOffenChronologisch[i].key == keyOfItem){
        return this.data.bestellungsListeOffenChronologisch[i];
      }
    }
  }

  showPreis(preis){
    if(preis < 0){
      return 0.00;
    }
    else {
      return preis;
    }

  }

  /**
   * Rechnet die gesamte Abrechnung mit allen Posten ab.
   */
  abrechnenGesamt() {
    var preis = 0.0;

    if (typeof this.item.einzelposten != 'undefined') {
      for (var i = 0; i < this.item.einzelposten.length; i++) {
        preis += (this.item.einzelposten[i].price * (this.item.einzelposten[i].anzahl - this.item.einzelposten[i].anzahlBezahlt));
      }
    }

    let alertWin = this.alertCtrl.create();
    alertWin.setTitle('Preis: ' + this.showPreis(preis.toFixed(2)));
    alertWin.addButton('Cancel');
    alertWin.addButton({
      text: 'OK',
      handler: data => {
        this.item.bezahlt = true;

        if (preis < 0){
          var anzahlTreuekarten = 0;
          for(var x = 0; x < this.item.einzelposten.length; x++){
            if(this.item.einzelposten[x].typ == 'treuekarte'){
              anzahlTreuekarten++;
            }
          }

         var substractorProKarte = preis / anzahlTreuekarten;

          for(var x = 0; x < this.item.einzelposten.length; x++){
            if(this.item.einzelposten[x].typ == 'treuekarte'){
             this.item.einzelposten[x].price -= substractorProKarte;
            }
          }

          this.data.updateBestellung(this.item);
        }

        if(this.item.einzelposten != null && typeof this.item.einzelposten != 'undefined') {
          for (var z = 0; z < this.item.einzelposten.length; z++) {
            if( this.item.einzelposten[z].abrechnungKellner == null || typeof this.item.einzelposten[z].abrechnungKellner == 'undefined'){
              console.log('erstellt');
              this.item.einzelposten[z].abrechnungKellner = [];
            }

            this.item.einzelposten[z].abrechnungKellner[this.item.einzelposten[z].abrechnungKellner.length] = {
              id: this.item.einzelposten[z].abrechnungKellner.length,
              name: this.data.currentUser.email,
              summe: (this.item.einzelposten[z].price * (this.item.einzelposten[z].anzahl -this.item.einzelposten[z].anzahlBezahlt)),
              anzahl: (this.item.einzelposten[z].anzahl - this.item.einzelposten[z].anzahlBezahlt),
              abgerechnet: false
            };
            this.item.einzelposten[z].anzahlBezahlt = this.item.einzelposten[z].anzahl;
          }
        }

        this.data.closeBestellung(this.item);
        this.data.getTischByPublicID(this.item.tischNr).then((res) => {
          this.aktuellerTisch = res;
          this.aktuellerTisch.besetzt = false;
          this.data.aendereTischstatus(this.aktuellerTisch);
        });

        this.navCtrl.pop();
      }
    });
    alertWin.present();
  }


  /**
   * Teilt die gesamte Abrechnungen durch eine Anzahl an Personen und rechent dann ab.
   */
  abrechnenGesamtSplit() {
    let prompt = this.alertCtrl.create({
      title: 'Abrechnung',
      message: "Gib die Anzahl der Personen an",
      inputs: [
        {
          name: 'anzahl',
          placeholder: 'Anzahl'
        },
      ],
      buttons: [
        {
          text: 'Abbrechen',
          handler: data => {
          }
        },
        {
          text: 'Abrechnen',
          handler: anzahlPersonen => {

            var preis = 0.0;

            if (typeof this.item.einzelposten != 'undefined') {
              for (var i = 0; i < this.item.einzelposten.length; i++) {
                preis += (this.item.einzelposten[i].price * (this.item.einzelposten[i].anzahl - this.item.einzelposten[i].anzahlBezahlt));
              }
            }

            let alert = this.alertCtrl.create();
            alert.setTitle('Preis pro Person: ' + this.showPreis((preis / parseInt(anzahlPersonen.anzahl)).toFixed(2))
              + "<br/>" + "<br/>" + "Es müssen " + anzahlPersonen.anzahl + " Personen zahlen");
            alert.addButton('Cancel');
            alert.addButton({
              text: 'OK',
              handler: data => {
                this.item.bezahlt = true;

                if (preis < 0){

                  var anzahlTreuekarten = 0;
                  for(var x = 0; x < this.item.einzelposten.length; x++){
                    if(this.item.einzelposten[x].typ == 'treuekarte'){
                      anzahlTreuekarten++;
                    }
                  }

                  var substractorProKarte = preis / anzahlTreuekarten;

                  for(var x = 0; x < this.item.einzelposten.length; x++){
                    if(this.item.einzelposten[x].typ == 'treuekarte'){
                      this.item.einzelposten[x].price -= substractorProKarte;
                    }
                  }

                  this.data.updateBestellung(this.item);
                }

                for (var z = 0; z < this.item.einzelposten.length; z++) {
                  if( this.item.einzelposten[z].abrechnungKellner == null || typeof this.item.einzelposten[z].abrechnungKellner == 'undefined'){
                    console.log('erstellt');
                    this.item.einzelposten[z].abrechnungKellner = [];
                  }

                  this.item.einzelposten[z].abrechnungKellner[this.item.einzelposten[z].abrechnungKellner.length] = {
                    id: this.item.einzelposten[z].abrechnungKellner.length,
                    name: this.data.currentUser.email,
                    summe: (this.item.einzelposten[z].price * (this.item.einzelposten[z].anzahl - this.item.einzelposten[z].anzahlBezahlt)),
                    anzahl: (this.item.einzelposten[z].anzahl - this.item.einzelposten[z].anzahlBezahlt),
                    abgerechnet: false
                  };

                  this.item.einzelposten[z].anzahlBezahlt = this.item.einzelposten[z].anzahl;
                }

                for (var z = 0; z < this.item.einzelposten.length; z++) {
                  this.item.einzelposten[z].anzahlBezahlt = this.item.einzelposten[z].anzahl;
                }
                this.data.closeBestellung(this.item);
                this.data.getTischByPublicID(this.item.tischNr).then((res) => {
                  this.aktuellerTisch = res;
                  this.aktuellerTisch.besetzt = false;
                  this.data.aendereTischstatus(this.aktuellerTisch);
                });
                this.navCtrl.pop();
              }
            });
            alert.present();
          }
        }
      ]
    });
    prompt.present();
  }

  changeEinzelpostenabrechnung() {
    this.einzelpostenAbrechnungAktiv = !this.einzelpostenAbrechnungAktiv;
    for (var i = 0; i < this.item.einzelposten.length; i++) {
      //Workaround um Objekte zu klonen
      this.einzelPostenListe.push(JSON.parse(JSON.stringify(this.item.einzelposten[i])));
    }
  }

  /**
   * Fügt einen Einzelnposten zu einer aktuellen Abrechnung hinzu.
   * @param obj
   */
  addToEinzelpostenZuBezahlen(obj) {

    //Workarround um item zu klonen, da ansonsten immer mit dem Objekt aus der Liste "einzelPostenliste gearbeitet wird.
    //An dieser Stelle soll das 'item' ein geklontes Objekt sein
    var item = JSON.parse(JSON.stringify(obj));
    if(item.anzahl-1 >= 0){
      for (var i = 0; i < this.einzelPostenListe.length; i++) {
        if (item.name == this.einzelPostenListe[i].name) {
          this.einzelPostenListe[i].anzahl = this.einzelPostenListe[i].anzahl - 1;
        }
      }

      if (this.einzelPostenZuBezahlen.length == 0) {
        this.einzelPostenZuBezahlen.push(item);
        this.einzelPostenZuBezahlen[0].anzahl = 1;
      } else {
        for (var k = 0; k < this.einzelPostenZuBezahlen.length; k++) {
          if (item.name == this.einzelPostenZuBezahlen[k].name) {
            this.einzelPostenZuBezahlen[k].anzahl = this.einzelPostenZuBezahlen[k].anzahl + 1;
            break;
          } else if (k == (this.einzelPostenZuBezahlen.length - 1) && this.einzelPostenZuBezahlen[k].name != item.name) {
            this.einzelPostenZuBezahlen.push(item);
            this.einzelPostenZuBezahlen[k + 1].anzahl = 1;
            break;
          }
        }
      }
    } else {
      alert('Dieser Posten ist bereits vollständig in der Rechnung!')
    }

  }

  /**
   * Rechnet die ausgewählten Einzelposten ab.
   */
  abrechnenZwischenBeleg() {
    var preis = 0.0;

    if (typeof this.einzelPostenZuBezahlen != 'undefined' && this.einzelPostenZuBezahlen != null) {
      for (var i = 0; i < this.einzelPostenZuBezahlen.length; i++) {
        preis += (this.einzelPostenZuBezahlen[i].price * this.einzelPostenZuBezahlen[i].anzahl);
      }

      let alerter = this.alertCtrl.create();
      alerter.setTitle('Preis: ' + this.showPreis(preis.toFixed(2)));
      alerter.addButton('Cancel');
      alerter.addButton({
        text: 'OK',
        handler: data => {
          this.markiereBezahltePosten();

          if (preis < 0){

            var anzahlTreuekarten = 0;
            for(var x = 0; x < this.einzelPostenZuBezahlen.length; x++){
              if(this.einzelPostenZuBezahlen[x].typ == 'treuekarte'){
                anzahlTreuekarten++;
              }
            }

            var substractorProKarte = preis / anzahlTreuekarten;

            for(var x = 0; x < this.einzelPostenZuBezahlen.length; x++){
              if(this.einzelPostenZuBezahlen[x].typ == 'treuekarte'){
                this.item.einzelposten[this.einzelPostenZuBezahlen[x].id].price -= substractorProKarte;
              }
            }

            this.data.updateBestellung(this.item);
          }

          this.data.zahleTeilBestellung(this.item).then(() => {
            if (this.checkIfAlleEinzelpostenBezahlt()) {
              this.item.bezahlt = true;
              this.data.closeBestellung(this.item);
              this.data.getTischByPublicID(this.item.tischNr).then((res) => {
                this.aktuellerTisch = res;
                this.aktuellerTisch.besetzt = false;
                this.data.aendereTischstatus(this.aktuellerTisch);
              })
            }
          });
          this.navCtrl.pop();
        }
      });
      alerter.present();

    } else {
      alert("Das ist etwas schief gelaufen")
    }
  }

  checkIfAlleEinzelpostenBezahlt() {
    for (var i = 0; i < this.item.einzelposten.length; i++) {
      if (!this.item.einzelposten[i].abgerechnet) {
        return false;
      }
    }
    return true;
  }

  markiereBezahltePosten() {
    for (var i = 0; i < this.einzelPostenZuBezahlen.length; i++) {
      for (var k = 0; k < this.item.einzelposten.length; k++) {

        //Bestellungen als bezahlt erfassen
        if (this.einzelPostenZuBezahlen[i].id == this.item.einzelposten[k].id) {
          this.item.einzelposten[k].anzahlBezahlt += this.einzelPostenZuBezahlen[i].anzahl;
          if (this.item.einzelposten[k].anzahl == this.item.einzelposten[k].anzahlBezahlt) {
            this.item.einzelposten[k].abgerechnet = true;
          }

          //Kellner Abrechnung
          if( this.item.einzelposten[k].abrechnungKellner == null || typeof this.item.einzelposten[k].abrechnungKellner == 'undefined'){
            console.log('erstellt');
            this.item.einzelposten[k].abrechnungKellner = [];
          }

          this.item.einzelposten[k].abrechnungKellner[this.item.einzelposten[k].abrechnungKellner.length] = {
            id: this.item.einzelposten[k].abrechnungKellner.length,
            name: this.data.currentUser.email,
            summe: (this.item.einzelposten[k].price * this.einzelPostenZuBezahlen[i].anzahl),
            anzahl: this.einzelPostenZuBezahlen[i].anzahl,
            abgerechnet: false
          }
        }
      }
    }
  }

  tischUmbuchen() {
    let alertWin = this.alertCtrl.create();
    alertWin.setTitle('Tisch auswählen');
    var that = this;
    for (var i = 0; i < this.data.tischList.length; i++) {
      if (!this.data.tischList[i].besetzt && this.data.tischList[i].verwendbar) {
        alertWin.addInput({
          type: 'radio',
          label: that.data.tischList[i].publicID + '',
          value: that.data.tischList[i],
          checked: false
        });
      }
    }


    alertWin.addButton('Abbrechen');
    alertWin.addButton({
      text: 'Umbuchen',
      handler: data => {
        if (data != '' && data != null && typeof data !== 'undefined' && typeof data.publicID !== 'undefined') {
          this.aktuellenTischFreigeben();
          this.item.tischNr = data.publicID;
          this.data.updateBestellung(that.item);
          this.data.getTischByPublicID(this.item.tischNr).then((res) => {
            this.aktuellerTisch = res;
            this.aktuellerTisch.besetzt = true;
            this.data.aendereTischstatus(this.aktuellerTisch);
          })
        } else {
          alert("Bitte Tisch auswählen!")
        }

      }
    });

    alertWin.present();
  }

  aktuellenTischFreigeben() {
    this.aktuellerTisch.besetzt = false;
    this.data.aendereTischstatus(this.aktuellerTisch);
  }

  addBestellung() {
    var tisch: any;
    this.dontDestroyPage = true;
    this.data.getTischByPublicID(this.item.tischNr).then((res) => {
      tisch = res;
      this.navCtrl.push(BestellungsAufnahmePage, {
        tisch: tisch,
        zuBestellunghHizufuegen: true,
        bestellung: this.item
      });
    })
  }

  ionViewDidLeave() {
    if (!this.dontDestroyPage) {
      this.navCtrl.popToRoot();
    }
    this.dontDestroyPage = false;
  }

  deletePosten(item) {
    let prompt = this.alertCtrl.create({
      title: 'Bestellung Stornieren?',
      message: 'Wollen Sie diesee Bestellung wirklich Stornieren? <br/> (' + item.name + ')',
      inputs: [
        {
          name: 'grund',
          placeholder: 'Grund'
        },
      ],
      buttons: [
        {
          text: 'Nein',
          handler: data => {
          }
        },
        {
          text: 'Ja',
          handler: data => {
            if (data.grund.length < 5) {
              alert("Sie müssen einen Grund angeben!")
            } else {
              if (item.anzahl > 0) {
                item.anzahl -= 1;
                if (item.anzahlBezahlt > 0) {
                  item.anzahlBezahlt -= 1;
                }

                if (item.anzahlInRechnung > 0) {
                  item.anzahlInRechnung -= 1;
                }

                if (item.anzahlBearbeitet > 0) {
                  item.anzahlBearbeitet -= 1;
                }

                if (item.anzahl == 0) {
                  this.item.einzelposten.splice(item.id, 1);
                  for (var i = 0; i < this.item.einzelposten.length; i++) {
                    this.item.einzelposten[i].id = i;
                  }
                }
                this.data.writeNewStorno(item, this.item, data.grund)
              }
            }

          }
        }
      ]
    });
    prompt.present();
  }

  /**
   * Teilt die Abrechnungen der ausgewählten Einzelposten durch eine Anzahl an Personen.
   */
  abrechnenZwischenBelegSplit() {

    let prompt = this.alertCtrl.create({
      title: 'Abrechnung',
      message: "Gib die Anzahl der Personen an",
      inputs: [
        {
          name: 'anzahl',
          placeholder: 'Anzahl'
        },
      ],
      buttons: [
        {
          text: 'Abbrechen',
          handler: data => {
          }
        },
        {
          text: 'Abrechnen',
          handler: anzahlPersonen => {
            var preis = 0.0;

            if (typeof this.einzelPostenZuBezahlen != 'undefined' && this.einzelPostenZuBezahlen != null) {
              for (var i = 0; i < this.einzelPostenZuBezahlen.length; i++) {
                preis += (this.einzelPostenZuBezahlen[i].price * this.einzelPostenZuBezahlen[i].anzahl);
              }

              let alerter = this.alertCtrl.create();
              alerter.setTitle('Preis pro Person: ' + this.showPreis((preis / parseInt(anzahlPersonen.anzahl)).toFixed(2)));
              alerter.addButton('Cancel');
              alerter.addButton({
                text: 'OK',
                handler: data => {
                  this.markiereBezahltePosten();

                  if (preis < 0){

                    var anzahlTreuekarten = 0;
                    for(var x = 0; x < this.einzelPostenZuBezahlen.length; x++){
                      if(this.einzelPostenZuBezahlen[x].typ == 'treuekarte'){
                        anzahlTreuekarten++;
                      }
                    }

                    var substractorProKarte = preis / anzahlTreuekarten;

                    for(var x = 0; x < this.einzelPostenZuBezahlen.length; x++){
                      if(this.einzelPostenZuBezahlen[x].typ == 'treuekarte'){
                        this.item.einzelposten[this.einzelPostenZuBezahlen[x].id].price -= substractorProKarte;
                      }
                    }

                    this.data.updateBestellung(this.item);
                  }

                  this.data.zahleTeilBestellung(this.item).then(() => {
                    if (this.checkIfAlleEinzelpostenBezahlt()) {
                      this.item.bezahlt = true;
                      this.data.closeBestellung(this.item);
                      this.data.getTischByPublicID(this.item.tischNr).then((res) => {
                        this.aktuellerTisch = res;
                        this.aktuellerTisch.besetzt = false;
                        this.data.aendereTischstatus(this.aktuellerTisch);
                      })
                    }
                  });
                  this.navCtrl.pop();
                }
              });
              alerter.present();
            } else {
              alert("Das ist etwas schief gelaufen")
            }
          }
        }
      ]
    });
    prompt.present();
  }

  addNeueTreuekarte() {
    this.barcodeScanner.scan().then((barcodeData) => {
      this.data.getKunde(barcodeData.text).then((kunde)=>{
        if(kunde == null){
          this.data.writeNewKunde(barcodeData.text);
          alert("Der Kunde hat nun einen Eintrag auf seiner digitalen Treuekarte")
        } else{
          console.log(JSON.stringify(kunde[0]));
          kunde[0].bonuskartenStand +=1;
          console.log(JSON.stringify(kunde[0]));
          // == 11, da es erst bei der 11. Bestellung einen Rabatt gibt
          if(kunde[0].bonuskartenStand == 11){
            var bestellung = {
              typ: "treuekarte",
              name: "Treuekarte",
              price: -10,
              bearbeitet: false,
              abgerechnet: false,
              anzahlBezahlt: 0,
              anzahlInRechnung: 0,
              id: this.item.einzelposten.length,
              anzahl: 1
            };

            kunde[0].bonuskartenStand = 0;
            this.item.einzelposten.push(bestellung);
            this.data.updateBestellung(this.item);
            alert("Der Kunde löst seine digitale Treuekarte ein!")
          } else {
            alert("Der Kunde hat " + kunde[0].bonuskartenStand + " Käufe auf seiner Treuekarte");
          }
          this.data.updateKunde(kunde[0]);
        }
      });
    });
  }


  addAlteTreueKarte(){
    var bestellung = {
      typ: "treuekarte",
      name: "Treuekarte",
      price: -10,
      bearbeitet: false,
      abgerechnet: false,
      anzahlBezahlt: 0,
      anzahlInRechnung: 0,
      id: this.item.einzelposten.length,
      anzahl: 1
    };

    this.item.einzelposten.push(bestellung);
    this.data.updateBestellung(this.item);
  }
}


