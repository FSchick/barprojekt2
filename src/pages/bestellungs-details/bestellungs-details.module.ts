import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BestellungsDetailsPage } from './bestellungs-details';

@NgModule({
  declarations: [
    BestellungsDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(BestellungsDetailsPage),
  ],
  exports: [
    BestellungsDetailsPage
  ]
})
export class BestellungsDetailsPageModule {}
