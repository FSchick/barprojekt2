import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {DataService} from "../../providers/data-service/data-service";
import {TabsPage} from "../tabs/tabs";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {FormBuilder, Validators, FormGroup} from "@angular/forms";
import {NativeStorage} from "@ionic-native/native-storage";


/**
 * Die Login-Seite dient der Authentifizierung des Users. Sobald er Authentifieziert ist, werden alle Daten initial geladen.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  private username = '';
  private password;
  allEmailAddresses = [];
  filterdEmailAddresses = [];
  showList = true;
  loginForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataService,
              private authService: AuthServiceProvider, private fb: FormBuilder, private alertCtrl: AlertController,
              private nativeStorage: NativeStorage, private platform: Platform) {
    if (!this.data.isFirebaseinit) {
      this.data.init();
    }
    this.loginForm = fb.group({
      'username': ['', Validators.compose([Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i)])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(8)])]
    });
    this.platform.ready().then(() => {
      this.nativeStorage.getItem('myitem')
        .then((data) => {
          if (data != null && typeof data != 'undefined') {
            this.allEmailAddresses = data.property
          }
        })

    })

  }

  validate(): boolean {
    if (this.loginForm.valid) {
      return true;
    }

    // figure out the error message
    var errorMsg = '';

    // validate each field
    var control = this.loginForm.controls['username'];
    if (!control.valid) {
      if (control.errors['required']) {
        errorMsg = 'Bitte geben sie den Nutzernamen an';
      } else if (control.errors['pattern']) {
        errorMsg = 'Falsches E-Mail Format';
      }
    }

    control = this.loginForm.controls['password'];
    if (!control.valid) {
      if (control.errors['required']) {
        errorMsg = 'Bitte geben sie dein Passwort ein';
      } else if (control.errors['minlength']) {
        errorMsg = 'Das Passwort muss mindestenss 6 Zeichen lang sein';
      }
    }

    var alert = this.alertCtrl.create({
      //title: 'Error!',
      subTitle: errorMsg || 'Leerer Fehler!!',
      buttons: ['OK']
    });
    alert.present();

    return false;
  }


  login() {
    if (this.validate()) {
      this.authService.login(this.username.toLowerCase(), this.password).then((bool) => {
        if (bool) {
          this.data.getCurrentUserOnStart().then((succuesful) => {
            if (succuesful) {
              this.navCtrl.push(TabsPage);
              this.username = "";
              this.password = "";
              this.data.getBestellungenOffen();
              this.data.getTische();
              this.data.getStornos();
              this.data.getTobaccos();
              this.data.getGetraenke();
              this.data.getSpeisen();
              this.data.getSchichten();
              this.data.getUsers();
              this.data.getAudio();
            } else {
              this.password = "";
              alert("Fehler beim Login mit Firebase")
            }
          })
        } else {
          this.password = "";
          alert("Fehler beim Login mit Firebase")
        }
      });
    }
  }

  getFilteredEmailAddresses() {
    this.username += "";
    if (this.allEmailAddresses.indexOf(this.username) == -1) {
      this.showList = true;
    } else {
      this.showList = false;
    }
    var list = [];
    for (var i = 0; i < this.allEmailAddresses.length; i++) {
      if (this.allEmailAddresses[i].toLowerCase().startsWith(this.username.toLowerCase())) {
        list.push(JSON.parse(JSON.stringify(this.allEmailAddresses[i])));
      }
    }
    this.filterdEmailAddresses = list;
  }

  setUserName(email) {
    this.username = email;
    this.showList = false;
  }
}
