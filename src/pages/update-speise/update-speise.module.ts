import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateSpeisePage } from './update-speise';

@NgModule({
  declarations: [
    UpdateSpeisePage,
  ],
  imports: [
    IonicPageModule.forChild(UpdateSpeisePage),
  ],
  exports: [
    UpdateSpeisePage
  ]
})
export class UpdateSpeisePageModule {}
