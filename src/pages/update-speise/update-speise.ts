import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DataService} from "../../providers/data-service/data-service";

/**
 * Generated class for the UpdateSpeisePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage({
  segment: '/tabs/produkte/update-speise/:speiseId'
})
@Component({
  selector: 'page-update-speise',
  templateUrl: 'update-speise.html',
})
export class UpdateSpeisePage {
  speiseId;
  speiseObject;
  speiseName;
  speisePreis;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataService) {
    this.speiseId = this.navParams.get("speiseId")
    this.speiseObject = this.getSpeiseById(this.speiseId)
    this.speisePreis = this.speiseObject.price;
    this.speiseName = this.speiseObject.name;

  }
  getSpeiseById(speiseId){
    for(var i = 0; i<this.data.speiseList.length; i++){
      if(this.data.speiseList[i].id == speiseId){
        return this.data.speiseList[i];
      }
    }
  }

  writeNewSpeise() {
    if (this.speiseName != null && this.speiseName != '' && this.speiseName != ' ') {
      this.data.writeOrUpdateSpeise(this.speiseId, this.speiseName, this.speisePreis.replace(',', '.'));
      this.navCtrl.pop();
    } else {
      alert('Bitte Namen angeben!')
    }
  }

}
