import {Component} from "@angular/core";
import {AlertController, App, IonicPage, NavController} from "ionic-angular";
import {DataService} from "../../providers/data-service/data-service";
import {BestellungsDetailsPage} from "../bestellungs-details/bestellungs-details";
import {BestellungsAufnahmePage} from "../bestellungs-aufnahme/bestellungs-aufnahme";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
/**
 *
 * Behandelt das initale Aufnehmen einer Bestellung.
 * Die genauere Logik wird in Bestellung Aufnahme behandelt.
 *
 */
@IonicPage({
  segment: 'bestellungen/'
})
@Component({
  selector: 'page-bestellungen',
  templateUrl: 'bestellungen.html',
})
export class BestellungenPage {
  constructor(private _app: App, public navCtrl: NavController, private data: DataService,
              private alertCtrl: AlertController, private authService: AuthServiceProvider) {

  }

  showBestellungsDetails(item) {
    this.navCtrl.push(BestellungsDetailsPage, {
      keyOfItem: item.key,
    })
  }

  addBestellung() {
    let alertWin = this.alertCtrl.create();
    alertWin.setTitle('Tisch auswählen');
    var that = this;
    for (var i = 0; i < this.data.tischList.length; i++) {
      if (!this.data.tischList[i].besetzt && this.data.tischList[i].verwendbar) {
        alertWin.addInput({
          type: 'radio',
          label: that.data.tischList[i].publicID + '',
          value: that.data.tischList[i],
          checked: false
        });
      }
    }


    alertWin.addButton('Abbrechen');
    alertWin.addButton({
      text: 'OK',
      handler: data => {
        if (data != '' && data != null) {
          this.navCtrl.push(BestellungsAufnahmePage, {
            tisch: data,
            zuBestellunghHizufuegen: false,
            bestellung: null
          });
        } else {
          alert("Bitte Tisch auswählen!")
        }

      }
    });
    alertWin.present();

  }

  isRelevantForUser(item) {
    return (typeof item != 'undefined');
  }

  logout() {
    this.authService.logout().then(() => {
      const root = this._app.getRootNav();
      root.popToRoot();
    });
  }

  ionSelected() {
    console.log("tab bestellungen ausgewählt")
  }

}
