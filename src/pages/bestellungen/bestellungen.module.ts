import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BestellungenPage } from './bestellungen';

@NgModule({
  declarations: [
    BestellungenPage,
  ],
  imports: [
    IonicPageModule.forChild(BestellungenPage),
  ],
  exports: [
    BestellungenPage
  ]
})
export class BestellungenPageModule {}
