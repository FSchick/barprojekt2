import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StornoPage } from './storno';

@NgModule({
  declarations: [
    StornoPage,
  ],
  imports: [
    IonicPageModule.forChild(StornoPage),
  ],
  exports: [
    StornoPage
  ]
})
export class StornoPageModule {}
