import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {DataService} from "../../providers/data-service/data-service";

/**
 * Zeigt dem Admin alle Stornos sortiert nach Datum an. Weiterhin können Details über die einzelnen stornierten Posten
 * angezeigt werden
 */
@IonicPage({
  segment: '/tabs/admin/storno'
})
@Component({
  selector: 'page-storno',
  templateUrl: 'storno.html',
})
export class StornoPage {

  headerDate;
  headerDateString;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataService, public alertCtrl: AlertController) {
  }

  isNextHeader(storno) {
    var index = this.data.stornoList.indexOf(storno);
    var stornoDatum = new Date(storno.timestamp);
    var stornoDatumString = this.dateToString(stornoDatum);
    stornoDatum = new Date(stornoDatumString);

    if (index == 0) {
      this.headerDate = stornoDatum;
      var header = this.dateToString(stornoDatum).split("/");
      this.headerDateString = header[2] + "-" + header[1] + "-" + header[0];
      return true;
    }

    if (stornoDatum.getTime() < this.headerDate.getTime()) {
      var header = this.dateToString(stornoDatum).split("/");
      this.headerDateString = header[2] + "-" + header[1] + "-" + header[0];
      this.headerDate = stornoDatum;
      return true;
    } else {
      return false;
    }
  }

  dateToString(date) {
    // TODO Anzeige mit Conform-Dialig und Header korrigieren
    var month = date.getMonth() + 1; //months from 1-12
    var day = date.getDate();
    var year = date.getFullYear();

    return year + "/" + month + "/" + day;
  }

  showStornoDetails(storno) {
    var str = "<h5>Grund:</h5>" + storno.grund + "<br/>"
      + "<h5>Benutzer</h5>" + storno.benutzer + "<br/>"
      + "<h5>Zeitpunkt</h5>" + new Date(storno.timestamp).toString();


    let alert = this.alertCtrl.create({
      title: storno.name,
      subTitle: str,
      buttons: ['OK']
    });
    alert.present();
  }
}
