import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DataService} from "../../providers/data-service/data-service";

/**
 * Generated class for the UpdateGetraenkPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage({
  segment: '/tabs/produkte/update-getaenk/:getraenkeId'
})
@Component({
  selector: 'page-update-getraenk',
  templateUrl: 'update-getraenk.html',
})
export class UpdateGetraenkPage {
  getraenkeId;
  getrankeObject
  getraenkName;
  getraenkPreis;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataService) {
      this.getraenkeId = navParams.get('getraenkeId');
      this.getrankeObject = this.getGetraenkById(this.getraenkeId)
      this.getraenkName = this.getrankeObject.name;
      this.getraenkPreis = this.getrankeObject.price;
  }

  getGetraenkById(getrankeId){
    for ( var i = 0; i<this.data.getraenkeList.length; i++){
      if(this.data.getraenkeList[i].id == getrankeId){
        return this.data.getraenkeList[i]
      }
    }
  }

  writeNewGetraenk() {
    if (this.getraenkName != null && this.getraenkName != '' && this.getraenkName != ' ') {
      this.data.writeOrUpdateGetraenk(this.getraenkeId, this.getraenkName, this.getraenkPreis.replace(',', '.'));
      this.navCtrl.pop();
    } else {
      alert('Bitte Namen angeben!')
    }
  }

}
