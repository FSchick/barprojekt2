import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateGetraenkPage } from './update-getraenk';

@NgModule({
  declarations: [
    UpdateGetraenkPage,
  ],
  imports: [
    IonicPageModule.forChild(UpdateGetraenkPage),
  ],
  exports: [
    UpdateGetraenkPage
  ]
})
export class UpdateGetraenkPageModule {}
