import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {DataService} from "../../providers/data-service/data-service";

/**
 * Zeigt dem Admin den Tagesumsatz an. Dabei geht ein Tag von 15:00 Uhr bis 15:00 Uhr (Minus eine Millisekunde).
 * Es wird der Wert der gesamten Bestellungen, der bereits bezahlten Bestellungen und der noch nicht bezahlten angezeigt.
 */
@IonicPage({
  segment: '/tabs/admin/tages-umsatz'
})
@Component({
  selector: 'page-tages-umsatz',
  templateUrl: 'tages-umsatz.html',
})
export class TagesUmsatzPage {
  tagesBestellungenGesamt;
  tagesBestellungenBezahlt;
  tagesBestellungenNichtBezahlt;
  eingeloesteTreueKarten;


  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataService) {
    this.calcTagesUmsatz();

  }

  calcTagesUmsatz() {
    var timeHours = new Date().getHours();
    var start;
    var end;

    var tmpDate = new Date();
    var monthDate = tmpDate.getMonth() + 1;
    var month = ("0" + monthDate).slice(-2);
    var day = ("0" + tmpDate.getDate()).slice(-2);

    //Da ein Geschäftstag von 15:00 Uhr bis 15:00 Uhr geht
    if (timeHours >= 15) {
      console.log('> 15Uhr');
      console.log(day + '.' + month);
      start = new Date(tmpDate.getFullYear() + "-" + month + "-" + day + "T15:00:00Z");
      end = new Date(tmpDate.getFullYear() + "-" + month + "-" + day + "T15:00:00Z");
      end.setDate(end.getDate() + 1);
    } else {
      console.log('< 15Uhr');
      console.log(day + '.' + month);
      end = new Date(tmpDate.getFullYear() + "-" + month + "-" + day + "T15:00:00Z");
      start = new Date(tmpDate.getFullYear() + "-" + month + "-" + day + "T15:00:00Z");
      start.setDate(start.getDate() - 1);
    }

    console.log('clc');
    this.data.getTagesUmsatz(start.getTime(), end.getTime() - 1).then((res) => {
      this.tagesBestellungenGesamt = res[0].toFixed(2);
      this.tagesBestellungenBezahlt = res[1].toFixed(2);
      this.tagesBestellungenNichtBezahlt = res[2].toFixed(2);
      this.eingeloesteTreueKarten = res[3];
    });
  }

}
