import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TagesUmsatzPage } from './tages-umsatz';

@NgModule({
  declarations: [
    TagesUmsatzPage,
  ],
  imports: [
    IonicPageModule.forChild(TagesUmsatzPage),
  ],
  exports: [
    TagesUmsatzPage
  ]
})
export class TagesUmsatzPageModule {}
