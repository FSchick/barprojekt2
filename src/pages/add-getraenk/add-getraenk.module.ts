import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddGetraenkPage } from './add-getraenk';

@NgModule({
  declarations: [
    AddGetraenkPage,
  ],
  imports: [
    IonicPageModule.forChild(AddGetraenkPage),
  ],
  exports: [
    AddGetraenkPage
  ]
})
export class AddGetraenkPageModule {}
