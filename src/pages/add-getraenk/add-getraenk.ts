import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {DataService} from "../../providers/data-service/data-service";

/**
 *
 * Der folgende Code behandelt das Hinzufügen eines Getränkes zur Datenbasis
 *
 */
@IonicPage({
  segment: '/tabs/produkte/add-getaenk'
})

@Component({
  selector: 'page-add-getraenk',
  templateUrl: 'add-getraenk.html',
})
export class AddGetraenkPage {
  getraenkName;
  getraenkPreis;
  title;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data: DataService) {
  }

  writeNewGetraenk() {
    if (this.getraenkName != null && this.getraenkName != '' && this.getraenkName != ' ') {
      this.data.writeOrUpdateGetraenk(null, this.getraenkName, this.getraenkPreis.replace(',', '.'));
      this.navCtrl.pop();
    } else {
      alert('Bitte Namen angeben!')
    }
  }


}
