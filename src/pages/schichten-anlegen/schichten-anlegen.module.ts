import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SchichtenAnlegenPage } from './schichten-anlegen';

@NgModule({
  declarations: [
    SchichtenAnlegenPage,
  ],
  imports: [
    IonicPageModule.forChild(SchichtenAnlegenPage),
  ],
  exports: [
    SchichtenAnlegenPage
  ]
})
export class SchichtenAnlegenPageModule {}
