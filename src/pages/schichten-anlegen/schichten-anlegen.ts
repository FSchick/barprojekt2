import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DataService} from "../../providers/data-service/data-service";

/**
 * Diese Seite dient dafür eine Schicht einzutragen.
 */
@IonicPage({
  segment: '/tabs/admin/schichten-verwalten/schichten-anlegen'
})
@Component({
  selector: 'page-schichten-anlegen',
  templateUrl: 'schichten-anlegen.html',
})
export class SchichtenAnlegenPage {
  private user;
  private timeStart;
  private timeEnd;
  private dateStart;
  private dateEnd;
  private schicht_;
  private header;

  constructor(public navCtrl: NavController, public navParams: NavParams, private data:DataService) {
    this.schicht_ = this.navParams.get('schichtToEdit');
    if(this.schicht_ != null){
      this.timeStart = this.schicht_.startTime;
      this.timeEnd = this.schicht_.endTime;
      this.dateStart = this.schicht_.startDate;
      this.dateEnd = this.schicht_.endDate;
      this.user = this.schicht_.user;
      this.header = 'bearbeiten';
    } else {
      this.header = 'anlegen'
    }
  }

  addSchicht(){
    if(this.header=='anlegen'){
      this.data.writeNewSchicht(this.timeStart, this.dateStart, this.timeEnd, this.dateEnd, this.user);
      this.navCtrl.pop();
    } else if(this.header == 'bearbeiten') {
      this.schicht_.startTime = this.timeStart;
      this.schicht_.endTime = this.timeEnd;
      this.schicht_.startDate = this.dateStart;
      this.schicht_.endDate = this.dateEnd;
      this.data.updateSchicht(this.schicht_);
      this.navCtrl.pop();
    }

  }
}
