import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

// if you've gone with the local installation approach, you'd use the following:
import firebase from 'firebase';
/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthServiceProvider {

  constructor() {

  }

  public login(username, password){
    return new Promise(function (resolve) {
      firebase.auth().signInWithEmailAndPassword(username, password).then(()=>{
        resolve(true);
        console.log('Logged In')
      }).catch(function(error) {
        console.log(error);
        resolve(false);
      });

    });
  }

  public logout(){
    return new Promise(function (resolve) {
      firebase.auth().signOut().then(function() {
        resolve(true);
        console.log('Logged Out')
      }).catch(function (error) {
        console.log(error);
        resolve(false);
      });
    })


  }

}
