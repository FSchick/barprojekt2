import {Injectable, NgZone} from '@angular/core';
import 'rxjs/add/operator/map';

// if you've gone with the local installation approach, you'd use the following:
import firebase from 'firebase';
import {AuthServiceProvider} from "../auth-service/auth-service";
import {NativeStorage} from "@ionic-native/native-storage";
import {Vibration} from "@ionic-native/vibration";
import {NativeRingtones} from '@ionic-native/native-ringtones';

@Injectable()
export class DataService {
  public db: any;
  public tobaccoList = [];
  public getraenkeList = [];
  public speiseList = [];
  public bestellungsListeOffenChronologisch = [];
  public bestellungsListeOffenAlphabetisch = [];
  public userList = [];
  public tischList = [];
  public stornoList = [];
  public schichtenList = [];
  public currentUser;
  public barList = [];
  private isInitialLoad = true;
  isFirebaseinit = false;
  fbAddUser;
  private tonURL = '';

  constructor(private authService: AuthServiceProvider, private nativeStorage: NativeStorage,
              private vibration: Vibration, private ringtones: NativeRingtones) {
  }

  zone: NgZone;

  init() {
    const fbConf = {
      apiKey: "AIzaSyB54mkSMNUU34CPpXL9Y948H_FF0LbAz4g",
      authDomain: "barprojekt2-6abd5.firebaseapp.com",
      databaseURL: "https://barprojekt2-6abd5.firebaseio.com",
      projectId: "barprojekt2-6abd5",
      storageBucket: "barprojekt2-6abd5.appspot.com",
      messagingSenderId: "138732047118"
    };
    firebase.initializeApp(fbConf);
    this.fbAddUser = firebase.initializeApp(fbConf, "Secondary");
    this.db = firebase.database().ref('/');
    this.isFirebaseinit = true;
    this.zone = new NgZone({});
  }

  getAudio() {
    var that = this;
    this.ringtones.getRingtone().then((ringtones) => {
      console.log(JSON.stringify(ringtones[0].Url))
      that.tonURL = ringtones[0].Url;
    });
  }


  //Verwendet zum Anlegen den sekündaren Zugriff auf Firebase.
  //Firebase loggt den Nutzer nach dem Erstellen automatisch um.
  //Durch die Nutzung der sekundären Verbindung wird der aktive Nutzer nicht rausgeschmissen.
  addUser(username, password, rolle, kasse) {
    var that = this;
    return new Promise(function (resolve) {
      that.fbAddUser.auth().createUserWithEmailAndPassword(username, password).then(() => {
        that.fbAddUser.auth().signOut();
        that.writeNewUser(username, rolle, kasse).then(() => {
          resolve(true);
        });
      }).catch(function (error) {
        resolve(false);
      });
    })
  }

  getCurrentUserOnStart() {
    var that = this;
    var user = firebase.auth().currentUser;
    return new Promise(function (resolve) {
      that.getUsers().then(() => {
        for (var i = 0; i < that.userList.length; i++) {
          if (that.userList[i].email == user.email) {
            that.currentUser = that.userList[i];
            that.userHasPermissions(user).then((hasPermission) => {
              if (hasPermission) {
                resolve(true)
              } else {
                that.authService.logout();
                resolve(false)
              }
            })
          }
        }
        if (that.currentUser == null) {
          resolve(false)
        }
      });
    })
  }

  getCurrentUserOnRun() {
    var user = firebase.auth().currentUser;
    for (var i = 0; i < this.userList.length; i++) {
      if (this.userList[i].email == user.email) {
        this.currentUser = this.userList[i];
        break;
      }
    }
    if (this.currentUser.rolle == "gesperrt") {
      this.authService.logout();
    }
  }

  userHasPermissions(fbUser) {
    var that = this;
    return new Promise(function (resolve) {
      for (var i = 0; i < that.userList.length; i++) {
        if (that.userList[i].email == fbUser.email) {
          that.currentUser = that.userList[i];
          break;
        }
      }
      if (that.currentUser.rolle == "gesperrt") {
        resolve(false);
      } else {
        resolve(true);
      }
    })
  }

  writeNewUser(email, rolle, bargeld) {
    return new Promise(function (resolve) {
      var newPostKey = firebase.database().ref('user/').push().key;
      firebase.database().ref('user/' + (newPostKey)).set({
        id: newPostKey,
        email: email,
        rolle: rolle,
        bargeld: bargeld,
        visible: true
      });
      resolve(true)
    })

  }


  writeOrUpdateTobacco(pk, name, description, hashtags, preis, verfügbar) {
    var newPostKey = null;
    if(pk == null){
      newPostKey = firebase.database().ref('tobacco/').push().key;
    } else {
      newPostKey = pk
    }
    firebase.database().ref('tobacco/' + (newPostKey)).set({
      id: newPostKey,
      name: name,
      description: description,
      hashtags: hashtags,
      price: preis,
      visible: verfügbar
    });
  }

  writeOrUpdateGetraenk(pk, name, price) {
    var newPostKey = null;
    if(pk == null){
      newPostKey = firebase.database().ref('getraenk/').push().key;
    } else {
      newPostKey = pk
    }
    firebase.database().ref('getraenk/' + (newPostKey)).set({
      id: newPostKey,
      name: name,
      price: price,
      visible: true
    });
  }

  writeOrUpdateSpeise(pk, name, price) {
    var newPostKey = null;
    if(pk == null){
      newPostKey = firebase.database().ref('speise/').push().key;
    } else {
      newPostKey = pk
    }
    firebase.database().ref('speise/' + (newPostKey)).set({
      id: newPostKey,
      name: name,
      price: price,
      visible: true
    });
  }

  writeNewKunde(name) {
    var newPostKey = firebase.database().ref('kunden/').push().key;
    firebase.database().ref('kunden/' + (newPostKey)).set({
      id: newPostKey,
      name: name,
      bonuskartenStand: 1
    });
  }

  getUsers() {
    var that = this;
    return new Promise(function (resolve) {
      var userDB = firebase.database().ref('user/');
      userDB.on('value', function (snapshot) {
        if (snapshot.val() != null) {
          that.updateUsers().then(() => {
            resolve(true);
          });
        } else {
          alert("Fehler beim Laden der Nutzer!")
          resolve(false);
        }
      })
    })
  }

  getTobaccos() {
    var that = this;
    var tobaccoDB = firebase.database().ref('tobacco/');
    tobaccoDB.on('value', function (snapshot) {
      that.updateTobaccos();
    })
  }

  getBars() {
    var that = this;
    return new Promise(function (resolve) {
      var barDB = firebase.database().ref('bar/');
      barDB.on('value', function (snapshot) {
        that.zone.run(() => {
          that.updateBars().then((bool) => {
            if (bool) {
              resolve(true)
            } else {
              resolve(false)
            }
          });
        })
      })
    })
  }

  getStornos() {
    var that = this;
    var tobaccoDB = firebase.database().ref('storno/');
    tobaccoDB.on('value', function (snapshot) {
      that.updateStornos();
    })
  }

  getGetraenke() {
    var that = this;
    var getraenkDB = firebase.database().ref('getraenk/');
    getraenkDB.on('value', function (snapshot) {
      that.updateGetraenke()
    })
  }

  getSpeisen() {
    var that = this;
    var getraenkDB = firebase.database().ref('speise/');
    getraenkDB.on('value', function (snapshot) {
      that.updateSpeisen();
    })
  }

  getSchichten() {
    var that = this;
    var schichtDB = firebase.database().ref('schicht/');
    schichtDB.on('value', function (snapshot) {
      that.updateSchichten();
    })
  }

  getKunde(name) {
    return new Promise(function (resolve) {
      var i = 0;
      var res = [];
      var query = firebase.database().ref("kunden/");
      query.orderByChild('name').startAt(name).endAt(name).once("value")
        .then(function (snapshot) {
          snapshot.forEach(function (childSnapshot) {
            res.push(childSnapshot.val());
            i++;
          });
          if (res.length == 1) {
            resolve(res);
          } else {
            resolve(null);
          }
        });
    })
  }

  updateUsers() {
    this.userList = [];
    var k = 0;
    var that = this;
    var query = firebase.database().ref("user/");
    return new Promise(function (resolve) {
      query.once("value")
        .then(function (snapshot) {
          snapshot.forEach(function (childSnapshot) {
            that.userList[k] = childSnapshot.val();
            k++;
          });
          that.getCurrentUserOnRun();
          var userEmailAddresses = [];
          for (var i = 0; i < that.userList.length; i++) {
            userEmailAddresses.push(that.userList[i].email);
          }
          that.nativeStorage.setItem('myitem', {property: userEmailAddresses})
            .then(
              () => console.log('Stored item!'),
              error => alert('Error storing item: ' + error)
            );
          resolve(true);
        });
    })
  }

  updateTobaccos() {
    this.tobaccoList = [];
    var i = 0;
    var that = this;
    var query = firebase.database().ref("tobacco/");
    query.once("value")
      .then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
          that.tobaccoList[i] = childSnapshot.val();
          i++;
        });
      });
  }

  updateBars() {
    this.barList = [];
    var i = 0;
    var that = this;
      return new Promise(function (resolve) {
        var query = firebase.database().ref("bar/");
        that.zone.run(() => {
          query.once("value")
            .then(function (snapshot) {
              snapshot.forEach(function (childSnapshot) {
                 that.barList[i] = childSnapshot.val();
                  that.barList[i].entfernung = '';
                  i++;
              });
              resolve(true);
            }).catch(function (err) {
            resolve(false)
          });
        })
      })
  }

  updateSchichten() {
    this.schichtenList = [];
    var i = 0;
    var that = this;
    var query = firebase.database().ref("schicht/");
    query.once("value")
      .then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
          that.schichtenList[i] = childSnapshot.val();
          i++;
        });
        function compare(a, b) {
          var schichtStringArrayA = a.startDate.split("-");
          var strSchichtDateFormattedA = schichtStringArrayA[1] + "-" + schichtStringArrayA[2] + "-" + schichtStringArrayA[0];

          var schichtStringArrayB = b.startDate.split("-");
          var strSchichtDateFormattedB = schichtStringArrayB[1] + "-" + schichtStringArrayB[2] + "-" + schichtStringArrayB[0];
          if (new Date(strSchichtDateFormattedA).getTime() < new Date(strSchichtDateFormattedB).getTime())
            return -1;
          if (new Date(strSchichtDateFormattedA).getTime() > new Date(strSchichtDateFormattedB).getTime())
            return 1;
          return 0;
        }

        that.schichtenList.sort(compare);
      });
  }

  updateStornos() {
    this.stornoList = [];
    var i = 0;
    var that = this;
    var query = firebase.database().ref("storno/");
    query.once("value")
      .then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
          that.stornoList[i] = childSnapshot.val();
          i++;
        });
        that.stornoList.reverse();
      });
  }

  updateGetraenke() {
    this.getraenkeList = [];
    var i = 0;
    var that = this;
    var query = firebase.database().ref("getraenk/");
    query.once("value")
      .then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
          that.getraenkeList[i] = childSnapshot.val();
          i++;
        });
      });
  }

  updateSpeisen() {
    this.speiseList = [];
    var i = 0;
    var that = this;
    var query = firebase.database().ref("speise/");
    query.once("value")
      .then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
          that.speiseList[i] = childSnapshot.val();
          i++;
        });
      });
  }

  getBestellungenOffen() {
    var that = this;
    var bestellungenDB = firebase.database().ref('bestellungenOffenProTisch/');
    bestellungenDB.on('value', function (snapshot) {
      that.zone.run(() => {
        that.updateBestellungen();
      })
    })
  }

  updateBestellungen() {
    var bestellungsListeVorher = JSON.parse(JSON.stringify(this.bestellungsListeOffenChronologisch))
    this.bestellungsListeOffenAlphabetisch = [];
    this.bestellungsListeOffenChronologisch = [];
    var k = 0;
    var that = this;
    var query = firebase.database().ref("bestellungenOffenProTisch/");
    query.orderByChild('bezahlt').startAt(false).endAt(false).once("value")
      .then(function (snapshot) {
        that.zone.run(() => {
          snapshot.forEach(function (childSnapshot) {
            that.bestellungsListeOffenChronologisch[k] = childSnapshot.val();
            that.bestellungsListeOffenAlphabetisch[k] = childSnapshot.val();
            k++;
          });

          if (that.currentUser.rolle == 'küche' && !that.isInitialLoad) {
            that.callRingAndVibrateIfNeccessary(bestellungsListeVorher);
          }
          that.isInitialLoad = false;
          function compare(a, b) {
            if (that.getTischNummerOnly(a) < that.getTischNummerOnly(b)) {
              console.log(that.getTischNummerOnly(a) + ' < ' + that.getTischNummerOnly(b))
              return -1;
            }
            if (that.getTischNummerOnly(a) > that.getTischNummerOnly(b)) {
              console.log(that.getTischNummerOnly(a) + ' > ' + that.getTischNummerOnly(b))
              return 1;
            }
            return 0;
          }

          that.bestellungsListeOffenAlphabetisch.sort(compare);
        });
      });
  }

  private getTischNummerOnly(bestellung) {
    if ((bestellung.tischNr + '').toLowerCase().startsWith('theke')) {
      var str = (bestellung.tischNr + '').replace(/ /g, '');
      str = str.replace('Theke', '')
      return parseInt(str);
    } else {
      return parseInt(bestellung.tischNr)
    }
  }

  private callRingAndVibrateIfNeccessary(bestellungsListeVorher: any) {
    for (var i = this.bestellungsListeOffenChronologisch.length - 1; i >= 0; i--) {
      var alteBestellung = this.getElementFromArrayByID(bestellungsListeVorher, this.bestellungsListeOffenChronologisch[i], 'key');
      if (alteBestellung == null && this.containsShishaPosten(this.bestellungsListeOffenChronologisch[i])) {
        this.ringAndVibate();
      } else if (this.containsShishaPosten(this.bestellungsListeOffenChronologisch[i])) {
        var shishasAusAlterbestellungsListe = this.getShishaFrom(alteBestellung)
        var shishaAusNeuerBestellungsliste = this.getShishaFrom(this.bestellungsListeOffenChronologisch[i])
        for (var j = shishaAusNeuerBestellungsliste.length - 1; j >= 0; j--) {
          var itemAusAlterBestellung = this.getElementFromArrayByID(shishasAusAlterbestellungsListe, shishaAusNeuerBestellungsliste[j], 'id')
          if (itemAusAlterBestellung == null) {
            this.ringAndVibate();
          } else if (itemAusAlterBestellung.anzahl < shishaAusNeuerBestellungsliste[j].anzahl) {
            this.ringAndVibate();
          }
        }
      }
    }
  }

  private ringAndVibate() {
    this.ringtones.playRingtone(this.tonURL);
    this.vibration.vibrate(1000);
  }

  containsShishaPosten(bestellung) {
    for (var i = 0; i < bestellung.einzelposten.length; i++) {
      if (bestellung.einzelposten[i].typ == 'shisha') {
        return true;
      }
    }
    return false;
  }

  getShishaFrom(bestellung) {
    var res = [];
    for (var i = 0; i < bestellung.einzelposten.length; i++) {
      if (bestellung.einzelposten[i].typ == 'shisha') {
        res.push(bestellung.einzelposten[i]);
      }
    }
    return res;
  }

  getElementFromArrayByID(array, item, id) {
    for (var i = 0; i < array.length; i++) {
      if (id == 'key') {
        if (item.key == array[i].key) {
          return array[i];
        }
      } else {
        if (item.id == array[i].id) {
          return array[i];
        }
      }
    }
    return null;
  }

  deleteTobacco(id) {
    var updates = {};
    updates['/tobacco/' + id.id] = null;
    firebase.database().ref().update(updates);
    this.updateTobaccos();
  }

  createBestellungGlobal(tischNr, bestellungsliste) {
    for (var i = 0; i < bestellungsliste.length; i++) {
      bestellungsliste[i].id = i;
    }
    var newPostKey = firebase.database().ref('bestellungenOffenProTisch/').push().key;
    firebase.database().ref('bestellungenOffenProTisch/' + newPostKey).set({
      key: newPostKey,
      tischNr: tischNr,
      einzelposten: bestellungsliste,
      timestamp: +new Date().setHours(new Date().getHours() + 2),
      bearbeitet: false,
      bezahlt: false,
      alleKellnerAbgerechnet: false
    });
  }

  closeBestellung(item) {
    var updates = {};
    updates['/bestellungenOffenProTisch/' + item.key] = item;
    firebase.database().ref().update(updates);

    //Dies ist nötig, da die Datenbanktabelle von Firebase automatisgit ch gelöscht wird, wenn sie keine Daten mehr beinhaltet.
    //Somit wird dieses Array durch den Listener nicht aktualisiert.
    if (this.bestellungsListeOffenChronologisch.length == 1 && this.bestellungsListeOffenChronologisch[0].bearbeitet) {
      this.bestellungsListeOffenChronologisch.pop()
    }
  }

  updateBestellung(item) {
    var updates = {};
    updates['/bestellungenOffenProTisch/' + item.key] = item;
    firebase.database().ref().update(updates);
  }

  zahleTeilBestellung(item) {
    return new Promise(function (resolve) {
      var updates = {};
      updates['/bestellungenOffenProTisch/' + item.key] = item;
      firebase.database().ref().update(updates).then(() => {
        resolve(true)
      });
    })
  }


  getTobaccoByName(name) {
    return new Promise(function (resolve) {
      var query = firebase.database().ref("tobacco/");
      query.orderByChild('/name').startAt(name).endAt(name).once("value")
        .then(function (snapshot) {
          snapshot.forEach(function (childSnapshot) {
            resolve(childSnapshot.val());
          });
        });
    })
  }

  updateTobaccoEigenschaften(item) {
    var updates = {};
    updates['/tobacco/' + item.id] = item;
    firebase.database().ref().update(updates);
  }

  writeNewTisch(publicID) {
    var newPostKey = firebase.database().ref('tisch/').push().key;
    firebase.database().ref('tisch/' + (newPostKey)).set({
      id: newPostKey,
      publicID: publicID,
      verwendbar: true,
      besetzt: false
    });
  }

  aendereTischstatus(item) {
    var updates = {};
    updates['/tisch/' + item.id] = item;
    firebase.database().ref().update(updates);
  }

  getTische() {
    var that = this;
    var tobaccoDB = firebase.database().ref('tisch/');
    tobaccoDB.on('value', function (snapshot) {
      that.updateTische();
    })
  }

  updateTische() {
    this.tischList = [];
    var i = 0;
    var that = this;
    var query = firebase.database().ref("tisch/");
    query.once("value")
      .then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
          that.tischList[i] = childSnapshot.val();
          i++;
        });
      });
  }

  getTischByPublicID(id) {
    var res = [];
    var i = 0;
    return new Promise(function (resolve) {
      var query = firebase.database().ref("tisch/");
      query.orderByChild('publicID').startAt(id).endAt(id).once("value")
        .then(function (snapshot) {
          snapshot.forEach(function (childSnapshot) {
            res[i] = childSnapshot.val();
            i++;
          });
          if (res.length == 1) {
            resolve(res[0]);
          }
        });
    })
  }

  aendereBenutzerRolle(user) {
    var that = this;
    return new Promise(function (resolve) {
      var updates = {};
      updates['/user/' + user.id] = user;
      var fbUser = firebase.auth().currentUser;
      if (fbUser.email == user.email) {
        alert("Sie können nicht die Berechtigungen ihres aktuellen Benutzers anpassen!")
        resolve(false);
      } else {
        firebase.database().ref().update(updates).then(() => {
          that.getCurrentUserOnStart().then((successful) => {
            if (successful) {
              resolve(true);
            } else {
              resolve(false);
            }
          });
        }).catch(function (error) {
          resolve(false)
        });
      }
    })
  }

  deleteBenutzer(userToDelete) {
    return new Promise(function (resolve) {
      userToDelete.rolle = "gesperrt";
      var updates = {};
      updates['/user/' + userToDelete.id] = userToDelete;
      firebase.database().ref().update(updates).then(() => {
        resolve(true)
      }).catch(function (error) {
        resolve(false)
      });
    })
  }

  writeNewStorno(item, bestellung, grund) {
    var that = this;
    this.updateBestellung(bestellung)
    var newPostKey = firebase.database().ref('storno/').push().key;
    firebase.database().ref('storno/' + (newPostKey)).set({
      id: newPostKey,
      bestellungsID: bestellung.key,
      name: item.name,
      preis: item.price,
      typ: item.typ,
      grund: grund,
      benutzer: that.currentUser.email,
      timestamp: +new Date()
    });
  }

  writeNewSchicht(startTime, startDate, endTime, endDate, user) {
    var newPostKey = firebase.database().ref('storno/').push().key;
    firebase.database().ref('schicht/' + (newPostKey)).set({
      id: newPostKey,
      startTime: startTime,
      startDate: startDate,
      endTime: endTime,
      endDate: endDate,
      user: user,
    });
  }

  updateSchicht(schicht) {
    var updates = {};
    updates['/schicht/' + schicht.id] = schicht;
    firebase.database().ref().update(updates);
  }


  getTagesUmsatz(start, end) {
    var res = [0, 0, 0, 0];
    var i = 0;
    console.log(start)
    console.log(end)
    console.log('called');
    return new Promise(function (resolve) {
      var query = firebase.database().ref("bestellungenOffenProTisch/");
      query.orderByChild('timestamp').startAt(start).endAt(end).once("value")
        .then(function (snapshot) {
          snapshot.forEach(function (childSnapshot) {
            console.log(childSnapshot);
            if (typeof childSnapshot.val().einzelposten != 'undefined') {
              console.log(childSnapshot.val().einzelposten.length)
              for (var k = 0; k < childSnapshot.val().einzelposten.length; k++) {
                console.log(k);
                res[0] += (childSnapshot.val().einzelposten[k].price * childSnapshot.val().einzelposten[k].anzahl);
                res[1] += (childSnapshot.val().einzelposten[k].price * childSnapshot.val().einzelposten[k].anzahlBezahlt);
                res[2] += (childSnapshot.val().einzelposten[k].price * (childSnapshot.val().einzelposten[k].anzahl - childSnapshot.val().einzelposten[k].anzahlBezahlt));
                if (childSnapshot.val().einzelposten[k].typ == 'treuekarte') {
                  res[3] += 1;
                }
              }
            }
            i++;
          });
          resolve(res);
        });
    })
  }

  deleteSchicht(id) {
    var updates = {};
    updates['/schicht/' + id.id] = null;
    firebase.database().ref().update(updates);
    this.updateTobaccos();
  }

  updateKunde(kunde) {
    var updates = {};
    updates['/kunden/' + kunde.id] = kunde;
    firebase.database().ref().update(updates);
  }

  getUmsatzKellner(email) {
    var res = [];
    var that = this;
    console.log('called');
    return new Promise(function (resolve) {
      var query = firebase.database().ref("bestellungenOffenProTisch/");
      query.orderByChild('alleKellnerAbgerechnet').startAt(false).endAt(false).once("value")
        .then(function (snapshot) {
          snapshot.forEach(function (childSnapshot) {
            var item = that.addItemToUmsatzKellner(childSnapshot, email, res);
            if (item != null) {
              res.push(item)
            }
          });
          console.log(res.length)
          resolve(res);
        });
    })
  }

  private addItemToUmsatzKellner(childSnapshot, email, res) {
    if (typeof childSnapshot.val().einzelposten != 'undefined') {
      for (var k = 0; k < childSnapshot.val().einzelposten.length; k++) {
        if (typeof childSnapshot.val().einzelposten[k].abrechnungKellner != 'undefined' && childSnapshot.val().einzelposten[k].abrechnungKellner != null) {
          for (var l = 0; l < childSnapshot.val().einzelposten[k].abrechnungKellner.length; l++) {
            if (childSnapshot.val().einzelposten[k].abrechnungKellner[l].name == email && !childSnapshot.val().einzelposten[k].abrechnungKellner[l].abgerechnet) {
              if (!this.checkIfListComtainsElement(res, childSnapshot.val())) {
                return childSnapshot.val();
              }
            }
          }
        }
      }
    }
  }

  checkIfListComtainsElement(list, element) {
    var erg = false;
    for (var i = 0; i < list.length; i++) {
      if (list[i].key == element.key) {
        erg = true;
      }
    }
    return erg;
  }

}
