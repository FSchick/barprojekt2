

BBBBBBBBBBBBBBBBB                                                 AAA
B::::::::::::::::B                                               A:::A
B::::::BBBBBB:::::B                                             A:::::A
BB:::::B     B:::::B                                           A:::::::A
  B::::B     B:::::B  aaaaaaaaaaaaa  rrrrr   rrrrrrrrr        A:::::::::A          ppppp   ppppppppp   ppppp   ppppppppp
  B::::B     B:::::B  a::::::::::::a r::::rrr:::::::::r      A:::::A:::::A         p::::ppp:::::::::p  p::::ppp:::::::::p
  B::::BBBBBB:::::B   aaaaaaaaa:::::ar:::::::::::::::::r    A:::::A A:::::A        p:::::::::::::::::p p:::::::::::::::::p
  B:::::::::::::BB             a::::arr::::::rrrrr::::::r  A:::::A   A:::::A       pp::::::ppppp::::::ppp::::::ppppp::::::p
  B::::BBBBBB:::::B     aaaaaaa:::::a r:::::r     r:::::r A:::::A     A:::::A       p:::::p     p:::::p p:::::p     p:::::p
  B::::B     B:::::B  aa::::::::::::a r:::::r     rrrrrrrA:::::AAAAAAAAA:::::A      p:::::p     p:::::p p:::::p     p:::::p
  B::::B     B:::::B a::::aaaa::::::a r:::::r           A:::::::::::::::::::::A     p:::::p     p:::::p p:::::p     p:::::p
  B::::B     B:::::Ba::::a    a:::::a r:::::r          A:::::AAAAAAAAAAAAA:::::A    p:::::p    p::::::p p:::::p    p::::::p
BB:::::BBBBBB::::::Ba::::a    a:::::a r:::::r         A:::::A             A:::::A   p:::::ppppp:::::::p p:::::ppppp:::::::p
B:::::::::::::::::B a:::::aaaa::::::a r:::::r        A:::::A               A:::::A  p::::::::::::::::p  p::::::::::::::::p
B::::::::::::::::B   a::::::::::aa:::ar:::::r       A:::::A                 A:::::A p::::::::::::::pp   p::::::::::::::pp
BBBBBBBBBBBBBBBBB     aaaaaaaaaa  aaaarrrrrrr      AAAAAAA                   AAAAAAAp::::::pppppppp     p::::::pppppppp
                                                                                    p:::::p             p:::::p
                                                                                    p:::::p             p:::::p
                                                                                   p:::::::p           p:::::::p
                                                                                   p:::::::p           p:::::::p
                                                                                   p:::::::p           p:::::::p
                                                                                   ppppppppp           ppppppppp

============================================================================================================================
Link zur Dokumentation:
https://barprojekt2-6abd5.firebaseapp.com/

============================================================================================================================
Individuelle Repositories:
Zu finden unter /Einzelabgaben/[Name des Studenten]/

============================================================================================================================
Wichtige Stellen im Quelltext:

  - Die Logik der Datenhaltung unter:
    + /src/prvoiders/data-service/

  - Die Aufnahme der Bestellung:
    + /src/pages/bestellungs-details/bestellungs-details.ts

  - Die Details einer Bestellung welche auch das Abrechnen behandelt:
    + /src/pages/bestellungs-aufnahme/bestellungs-aufnahme.ts
