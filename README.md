### Dies ist das Projekt von Felix Kohlen, Kevin Loncsarszky und Fabian Schick
Die Präsentation kann unter folgenden Link aufgerufen werden: https://barprojekt2-6abd5.firebaseapp.com/

Die Einzelabgaben befinden sich ebenfalls in diesem Repository.
### Das Projekt starten:

```bash
$ git clone
$ sudo npm install 
$ ionic cordova run browser
```
