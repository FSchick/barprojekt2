## In diesem Ordner befinden sich die Test-Apps von Fabian Schick aus dem f�nften Semester der Vorlesung Mobile-Apps

In dem Ordner MyFirstApp befindet sich eine App, welche auf dem Android-Starter Tutorial basieriert. Sie muss daher �ber Android Studion gestartet werden. Hardware Plugins oder �hnliches werden hier nicht verwendet.

In dem Ordner myFirstCordovaApp befindet sich eine App, in der zuerst eine Abfrage nach dem Fingerabdruck erfolgt (Funtioniert nur unter Android). Da dies im Brwoser nicht m�glich ist, wird dieser Teil auf der Platform �bersprungen. Anschlie�end ist es �ber den Button mit dem Namen "Click Me" m�glich, die Kamera zu aktivieren.

In dem Ordner CordovaTut befindet sich eine App, in der einige Plugins verwendet werden k�nnen. Diese Anwendung diente nur dazu, den Umgang mit den Plugins zu erlernen. Die verwendeten Plugins stellen die folgenden Funtionen bereit:
```bash
* Batterie-Status
* Gelocation
* BarcodeScanner
* Mail
* LocalStorage
* Netzwerkverbindungen
* Geofence
* Kamera (Funtioniert in dieser Anwendung nicht wirklich. Daher wurde diese Funktionalit�t in die App myFirstCordovaTut verschoben, um sie gesondert auszuprobieren)
```

In dem Ordner ReactNative wird das ReactNative Starter-Tutorial ausprobiert.