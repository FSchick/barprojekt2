var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        document.getElementById("buttonWarnung").addEventListener("click", showAlert);
        document.getElementById("buttonGeo").addEventListener("click", showGeo);
        document.getElementById("buttonCamera").addEventListener("click", makeFoto);
        document.getElementById("buttonSave").addEventListener("click", saveFormData);
        document.getElementById("buttonSocial").addEventListener("click", socialShare);
        document.getElementById("buttonLoadData").addEventListener("click", loadData);
        document.getElementById("buttonClearData").addEventListener("click", clearData);
        document.getElementById("buttonGeoFence").addEventListener("click", makeGeo);
        document.getElementById("buttonClearGeofence").addEventListener("click", clearGeofence);
        window.addEventListener("orientationchange", orientationChanger);

        //Geofence
        window.geofence.initialize().then(function() {
            console.log("Successful initialization");
        }, function(error) {
            console.log("Error", error);
        });

        window.geofence.onTransitionReceived = function(geofences) {
            geofences.forEach(function(geo) {
                console.log('Geofence transition detected + angekommen!', geo);
            });
        };

        // Initialize app
        var myApp = new Framework7();

        // If we need to use custom DOM library, let's save it to $$ variable:
        var $$ = Dom7;

        // Add view
        var mainView = myApp.addView('.view-main', {
            // Because we want to use dynamic navbar, we need to enable it for this view:
            dynamicNavbar: true
        });

        myApp.onPageInit('about', function(page) {
            myApp.alert('Here comes About page');
        })
    },
};

app.initialize();

function socialShare() {
    window.plugins.socialsharing.shareWithOptions(options, onSuccessSocial, onErrorSocial);
}
// this is the complete list of currently supported params you can pass to the plugin (all optional)
var options = {
    message: 'share this', // not supported on some apps (Facebook, Instagram)
    subject: 'the subject', // fi. for email
    files: ['', ''], // an array of filenames either locally or remotely
    url: 'https://www.website.com/foo/#bar?a=b',
    chooserTitle: 'Pick an app' // Android only, you can override the default share sheet title
}

var onSuccessSocial = function(result) {
    console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
    console.log("Shared to app: " + result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
}

var onErrorSocial = function(msg) {
    console.log("Sharing failed with message: " + msg);
}


function saveFormData() {
    var formData = document.getElementById('name').value
    if (typeof(Storage) !== "undefined") {
        localStorage.setItem("meinname", formData);
        document.getElementById('name').value = "";
    }
}

function loadData() {
    alert(localStorage.getItem("meinname"))
}

function clearData() {
    if (localStorage.getItem("meinname") != null) {
        localStorage.setItem("meinname", null);
    }
}

function showAlert() {
    navigator.notification.alert(
        'The cake is a lie!', // message
        alertDismissed, // callback
        'Portal', // title
        'NooOoOOoOOo :(' // buttonName
    );
}

function alertDismissed() {

}

function showGeo() {
    navigator.geolocation.getCurrentPosition(onSuccess, onError);
}

var onSuccess = function(position) {
    alert('Breitengrad: ' + position.coords.latitude + '\n' +
        'Längengrad: ' + position.coords.longitude + '\n' +
        'Höhe: ' + position.coords.altitude + '\n' +
        'Genauigkeit: ' + position.coords.accuracy + '\n' +
        'Genauigkeit der Höhe: ' + position.coords.altitudeAccuracy + '\n' +
        'Richtiung: ' + position.coords.heading + '\n' +
        'Geschwindigkeit: ' + position.coords.speed + '\n' +
        'Zeitstempel: ' + position.timestamp + '\n');
};

function onError(error) {
    alert('code: ' + error.code + '\n' +
        'message: ' + error.message + '\n');
}

function makeFoto() {
    navigator.camera.getPicture(cameraSuccess, cameraError, {
        quality: 100
    });
}

function cameraSuccess(imageData) {
    console.log("success")
    var image = document.getElementById('myImage');
    image.src = "data:image/jpeg;base64," + imageData;
}

function cameraError(message) {
    alert('Failed because: ' + message);
}

function orientationChanger() {
    var currentOrientation = "";

    if (window.orientation == 0) {
        currentOrientation = "portrait";
    } else if (window.orientation == 90) {
        currentOrientation = "landscape";
    } else if (window.orientation == -90) {
        currentOrientation = "landscape";
    } else if (window.orientation == 180) {
        currentOrientation = "portrait";
    }
    document.getElementById("status").innerHTML = currentOrientation;
}

function makeGeo() {
    window.geofence.addOrUpdate({
        id: "fence1",
        latitude: 50.2980049,
        longitude: 18.6593152,
        radius: 3000,
        transitionType: TransitionType.ENTER,
        notification: {
            id: 1,
            title: "Welcome in Gliwice",
            text: "You just arrived to Gliwice city center.",
            openAppOnClick: true
        }
    }).then(function() {
        console.log('Geofence successfully added');
    }, function(reason) {
        console.log('Adding geofence failed', reason);
    })
}

function clearGeofence() {
    function removeAllGeofences() {
        window.geofence.removeAll()
            .then(function() {
                console.log('All geofences successfully removed.');
            }, function(reason) {
                console.log('Removing geofences failed', reason);
            });
    }
}
