package test.helloworldandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Festlegen der TextBox
        editText = (EditText) findViewById(R.id.edit_message);
    }

    /**
     * Called when the user clicks the Send button
     */
    public void sendMessage(View view) {
        if (checkError()) {
            Intent intent = new Intent(this, DisplayMessageActivity.class);
            String message = editText.getText().toString();
            intent.putExtra(EXTRA_MESSAGE, message);
            startActivity(intent);
        }
    }

    //Wird aufgerufen wenn der Nutzer den Button sendToast clickt
    public void sendToast(View view) {
        if (checkError()) {
            String message = editText.getText().toString();
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        }
    }

    //Prüfen ob die TextBox leer ist
    public boolean checkError(){
        if(editText.length() == 0){
            editText.setError(getApplicationContext().getResources().getString(R.string.error_message));
            return false;
        } else{
            return true;
        }

    }
}
